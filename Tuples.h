/*
 * Tuples.h
 *
 *  Created on: Mar 15, 2012
 *      Author: sergeyn
 */

#ifndef TUPLES_H_
#define TUPLES_H_

#include "globals.h"
#include "Geo.h"

struct lexTuple {
	std::string term;
	bignum address;
	unsigned int sizeDocs;
	unsigned int sizeFreqs;
	unsigned int count;
	explicit lexTuple(const std::string& t="", bignum ad=0, unsigned int szd=0, unsigned int szf=0, unsigned int cnt=0) :
														term(t),address(ad),sizeDocs(szd),sizeFreqs(szf), count(cnt) {}
	static lexTuple fromString(const std::string& chunk);
};

std::ostream& operator<< (std::ostream& s, const lexTuple& t);

//======================================================================================================
class termTuple { //hold term rel. data
public:
	bignum docid;
	unsigned int freq;
	std::string term;

	termTuple(const std::string& t="", bignum d=0, unsigned int f=0) : docid(d),freq(f),term(t) {}
	bool operator<(const termTuple& rhs) const { return (term == rhs.term) ? docid < rhs.docid : term<rhs.term; }
	static termTuple fromString(const std::string& chunk);
};

std::ostream& operator<< (std::ostream& s, const termTuple& t);
//======================================================================================================
class docTuple {
public:
	bignum docid;
	//std::string label;
	std::string url;
	unsigned seconds;
	//unsigned int size;
	gPoint location;

	docTuple(bignum d=0, unsigned int sz=0, std::string u="", std::string l="", tRange::timeType sec=0) :
		docid(d),/*label(l),*/url(u),seconds(sec)/*,size(0)*/ {}
	static docTuple fromString(const std::string& chunk);
	template<typename Iterator>
	static void sortByDocid(Iterator begin, Iterator end) { std::sort(begin,end,[](const docTuple& a, const docTuple& b){ return a.docid < b.docid; } ); }
	template<typename Iterator>
	static void sortBySeconds(Iterator begin, Iterator end) { std::sort(begin,end,[](const docTuple& a, const docTuple& b){ return a.seconds < b.seconds; } ); }
	template<typename Iterator>
	static void sortByX(Iterator begin, Iterator end) { std::sort(begin,end,[](const docTuple& a, const docTuple& b){ return a.location.x_crd < b.location.x_crd; } ); }
	template<typename Iterator>
	static void sortByY(Iterator begin, Iterator end) { std::sort(begin,end,[](const docTuple& a, const docTuple& b){ return a.location.y_crd < b.location.y_crd; } ); }

	static std::string createKey(std::vector<docTuple>::iterator begin, std::vector<docTuple>::iterator end) {
		std::stringstream ss;
		ss << begin->location << end->location << (end-begin); //create a key
		return ss.str();
	}
};
std::ostream& operator<< (std::ostream& s, const docTuple& t);
//======================================================================================================
/*
class completeTweetTuple : public docTuple{
public:
	std::vector<std::string> terms;
	completeTweetTuple() {}
	explicit completeTweetTuple(const docTuple& dc) : docTuple(dc) {}
	completeTweetTuple(const docTuple& dc, const std::vector<std::string>& v) : docTuple(dc), terms(v) {}


};*/

class queryTuple {
public:
	std::vector<std::string> terms;
	gRange geoR;
	tRange timeR;
	queryTuple() : geoR(USABBOX), timeR(ALLTIME) {}
	explicit queryTuple(const std::vector<std::string>& ts, gRange gr = USABBOX, tRange tr = ALLTIME) : terms(ts), geoR(gr), timeR(tr) {}
	static queryTuple fromString(const std::string& chunk);
	static queryTuple fromWebString(std::string& chunk);
};
std::ostream& operator<< (std::ostream& s, const queryTuple& t);

//======================================================================================================
class result {
public:
	bignum did;
	gPoint location;
	float score;
	std::string url;
	tRange::timeType timestamp;
	result(bignum d=0, float s=0.0, gPoint loc=gPoint(), tRange::timeType ts=0) : did(d),location(loc), score(s), timestamp(ts){}
	bool operator>(const result& rhs) { return score>rhs.score; }
	bool operator<(const result& rhs) { return score<rhs.score; }
};
std::ostream& operator<< (std::ostream& s, const result& t);
typedef std::vector<result> results;
//======================================================================================================

class doclenMap : public std::vector<docTuple> {
public:
	doclenMap() {}
	std::unordered_map<bignum,docTuple> hashmapping; //it is either hash or vector not both
	docTuple& getDocMeta(bignum did) { return hashmapping[did]; }
	explicit doclenMap(const std::string fname);
};

class lexicon : public std::unordered_map<std::string,lexTuple>
{
public:
	lexicon() {}
	explicit lexicon(const std::string fname);
};

#endif /* TUPLES_H_ */

