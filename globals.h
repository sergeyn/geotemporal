/*
 * globals.h
 *
 *  Created on: Mar 11, 2012
 *      Author: sergeyn
 */

#ifndef GLOBALS_H_
#define GLOBALS_H_

#include "registry.h"

typedef unsigned int bignum;
typedef std::unordered_map<std::string,unsigned int> mapStr2UI;
typedef std::unordered_map<std::string,bignum> mapStr2bignum;
const unsigned int MRGTREADSCOUNT(16);

#define PARSETYPES AsciiStreamer,TweetParser
#define COMPRESSTYPES  BlockedVarByteForBignum,CompressTweetFreqs
#define INVLIST CompressedBlockedList

#define THROW(w) {std::cerr << w << std::endl; abort(); }

template <typename T>
inline std::string int2s(T i) {
	std::stringstream s;
	s<<i;
	return s.str();
}

template <typename T>
std::ostream& operator<< (std::ostream& s, const std::vector<T>& t) {
	for(auto it=t.begin(); it!=t.end(); ++it)
		s << *it << ' ';
	return s;
}

std::vector<std::string> tokenize(const std::string& str,
	      const std::string& delimiters = " ", const bool trimEmpty = true);

class StreamerOpenError {
public:
std::string name;
StreamerOpenError(const std::string& n):name(n){
	std::cerr << "failed to open " << n << std::endl;}
};
class BaseStreamer {
protected:
	const std::string fname;
public:
	BaseStreamer(const std::string& f) : fname(f){}
	virtual ~BaseStreamer(){}
	virtual std::string getChunk() = 0;
	std::string getFname() const {return fname;}
};

template <typename S>
class GenericStreamer : public BaseStreamer{
protected:
	S handle;
public:
	explicit GenericStreamer(const std::string& f) : BaseStreamer(f), handle(f) {
		  //if (! handle.is_open())
		if (! handle.good())
			THROW(f + " can't open");
	}
	virtual ~GenericStreamer() { //handle.close();

	}

	//return empty string when done...
	virtual std::string getChunk() {
		if(! handle.good())
			return "";
		std::string tmp;
		std::getline(handle, tmp);
		return tmp;
	}

};

typedef  GenericStreamer<std::ifstream> AsciiStreamer;
typedef  GenericStreamer<std::stringstream> StringStreamer;

#endif /* GLOBALS_H_ */
