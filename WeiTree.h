/*
 * WeiTree.h
 *
 *  Created on: Apr 20, 2012
 *      Author: sergeyn
 */

#ifndef WEITREE_H_
#define WEITREE_H_

#include "globals.h"
#include "Geo.h"
#include <string>

const size_t stopDividingAt(256*1024*1024);
const std::string rootKey("rootWTree");


template <typename Data, typename DataContainer>
class WeiTree {
protected:
	gRange rootRange;
	unsigned int depth;
	std::vector<WeiTree*> children;
	DataContainer* cached;
	std::string indexDir;

	WeiTree(const WeiTree& w) { throw "!"; }

public:
	WeiTree(unsigned int d=0, const std::string& rawStorage="") : depth(d), cached(nullptr)  {
		init(d,rawStorage);
	}
//--------------------------------------------------------------------------------------------
	void init(unsigned int d=0, const std::string& rawStorage="", gRange rr=gRange()) {
		depth = d;
		rootRange = rr;
		if(depth==0) {
			indexDir = rawStorage.substr(0,rawStorage.find_last_of("/\\"));
			registry::getInstance()[rootKey]=indexDir;
		}
	}
//--------------------------------------------------------------------------------------------
	~WeiTree() {
		if(cached)
			delete cached;
		for(WeiTree* child : children)
			delete child;
	}
//--------------------------------------------------------------------------------------------
	WeiTree& operator=(WeiTree& w) {
		if(cached!=nullptr || children.size())
			throw "oi, wei";
		if(&w == this)
			return *this;
		children = w.children;
		cached = w.cached;
		w.cached = 0; // we can't have it delete our ptr
		w.children.clear(); //or kill our children
		rootRange = w.rootRange;
		depth = w.depth;
		return *this;
	}
//--------------------------------------------------------------------------------------------
	inline static unsigned int getConstForDepth(unsigned int depth) { return depth>=2 ? 4 : 4; }
//--------------------------------------------------------------------------------------------
	inline size_t numChildren() const { return children.size(); }
//--------------------------------------------------------------------------------------------
	std::vector<std::string> getAllLeafPaths() const {
		if(children.empty()) //leaf node
			return { indexDir };
		std::vector<std::string> ret;
		for(auto c : children) {
			std::vector<std::string> v = c->getAllLeafPaths();
			for(std::string& s : v)
				ret.push_back(s);
		}
		return ret;
	}
	//--------------------------------------------------------------------------------------------
	std::vector<std::string> getIntersectingLeafPaths(gRange q) const {
		gRange::intersectStatus st1 = rootRange.howIntersects(q);
		gRange::intersectStatus st2 = q.howIntersects(rootRange);
		if(st1 == gRange::OUTSIDE && st2 == gRange::OUTSIDE)
			return {};
		if(children.empty()) //leaf node
			return { indexDir };
		std::vector<std::string> ret;
		for(auto c : children) {
			std::vector<std::string> v = c->getIntersectingLeafPaths(q);
			for(std::string& s : v)
				ret.push_back(s);
		}
		return ret;
	}
//--------------------------------------------------------------------------------------------
	inline size_t numPosts() const {
		size_t sum = 0;
		for(const auto& n : children)
			sum += n->numPosts();
		return sum + numPostsCached();
	}
//--------------------------------------------------------------------------------------------
	static WeiTree* loadFromStream(std::istream& in, bignum& totalDocs) {
		bignum nChild, nposts;
		float x,y,z,w;
		std::string dir;
		std::vector<WeiTree*> lstack;
		std::vector<unsigned int > vstack;
		totalDocs = 0;
		vstack.push_back(0);
		WeiTree* rroot = new WeiTree(vstack.back());
		lstack.push_back(rroot);
		while(lstack.size()) {
			if(lstack.size()>100)
				THROW("too d");
			WeiTree* root = lstack.back();
			lstack.pop_back();
			in >> nChild >> nposts  >> x >> y >> z >> w >> dir;
			totalDocs += nposts;
			root->init(vstack.back(),dir,gRange(gPoint(x,y),gPoint(z,w)));
			root->indexDir = dir;
			for(unsigned int i=0; i<nChild; ++i) {
				vstack.push_back(root->depth + 1);
				WeiTree* ptr = new WeiTree(root->depth + 1);
				lstack.push_back(ptr);
				root->children.push_back(ptr);
			}

			vstack.pop_back();
		}
		return rroot;
	}
//--------------------------------------------------------------------------------------------
	void printTree(std::ostream& out=std::cout) const { //	./test | cut -d':' -f5 | tr -d '\n'
		out << numChildren() << " " << numPostsCached() << " " << rootRange << " " << indexDir << std::endl;
		for(const auto& n : children)
			n->printTree(out);
	}
//--------------------------------------------------------------------------------------------
	inline size_t numPostsCached() const { return (cached == nullptr ) ? 0 : cached->size(); }
//--------------------------------------------------------------------------------------------
	void updateNodes(std::vector<Data>& data, size_t begin, size_t end) { }
//--------------------------------------------------------------------------------------------
	void buildTree(gRange range, std::vector<Data>& data, size_t begin, size_t end) {
		assert(children.empty() && cached == nullptr); //should not work on an updated structure

		if(depth==0) { //root level sanitation
			size_t errs = 0;
			for(Data& d : data)
				if( (!range.isPointInRange(d.location)) || d.location.isnull() ) {
					++errs;
					d.location = SALTLAKE;
					//d = data.back();
					//data.pop_back();
				}
			std::cerr << errs << " locations were outside of our root range! moved them to SALTLAKE..." << std::endl;
			//end -= errs;
		}

		const size_t sz = end-begin;
		auto itBegin = data.begin()+begin;
		auto itEnd = data.begin()+end+1;

		rootRange = range;
		if(sz < stopDividingAt) { //stopping cond.
			if(depth==0)
				registry::getInstance()[Data::createKey(itBegin,itEnd)] = registry::getInstance()[rootKey]; //register this node
			std::cout << "warning single hack!" << std::endl;	
			cached = new DataContainer(data); //itBegin,itEnd); //just push the data to leaf node (it was registered already)
			return;
		}

		std::string currentKey = Data::createKey(itBegin,itEnd);
		if(depth%2)
			Data::sortByX(itBegin,itEnd);
		else
			Data::sortByY(itBegin,itEnd);


		if(depth==0) {
			currentKey = Data::createKey(itBegin,itEnd);
			registry::getInstance()[currentKey] = registry::getInstance()[rootKey]; //register this node
			std::cout << "root:" << currentKey << " " <<  registry::getInstance()[currentKey] <<std::endl;
		}

		const size_t numChildren = getConstForDepth(depth);
		assert(numChildren>1);
		const size_t bucket_sz = sz / numChildren;
		children.resize(numChildren);
		for(unsigned int i=0; i<numChildren; ++i)
			children[i] = new WeiTree(depth+1); //create children nodes

		for(unsigned int i = 0; i<numChildren-1; ++i) { //build trees in children
			std::cout << i << " in* " << begin << std::endl;
			const size_t childBegin = begin+(i*bucket_sz);
			const size_t childEnd = begin+(((1+i)*bucket_sz)-1);
			gRange childR = (depth%2) ?
					gRange(gPoint(data[childBegin].location.x_crd ,range.first.y_crd),gPoint(data[childEnd].location.x_crd,range.second.y_crd)) :
					gRange(gPoint(range.first.x_crd,data[childBegin].location.y_crd ),gPoint(range.second.x_crd,data[childEnd].location.y_crd)) ;
			children[i]->indexDir = registry::getInstance()[currentKey]+"/"+int2s(i);
			registry::getInstance()[ Data::createKey(data.begin()+childBegin,data.begin()+childEnd+1)] = children[i]->indexDir;
			children[i]->buildTree(childR, data,childBegin,childEnd);
		}
		const size_t childBegin = begin+(bucket_sz*(numChildren-1));
		const size_t childEnd = end;
		gRange childR = (depth%2) ?
				gRange(gPoint(data[childBegin].location.x_crd ,range.first.y_crd),gPoint(data[childEnd].location.x_crd,range.second.y_crd)) :
				gRange(gPoint(range.first.x_crd, data[childBegin].location.y_crd ),gPoint(range.second.x_crd,data[childEnd].location.y_crd)) ;

		unsigned int i = numChildren-1;
		children[i]->indexDir = registry::getInstance()[currentKey]+"/"+int2s(i);
		registry::getInstance()[ Data::createKey(data.begin()+childBegin,data.begin()+childEnd+1)] = children[i]->indexDir;
		children[i]->buildTree(childR, data,childBegin,childEnd);
	}
//--------------------------------------------------------------------------------------------
};

#endif /* WEITREE_H_ */
