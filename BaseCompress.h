/*
 * BaseCompress.h
 *
 *  Created on: Mar 13, 2012
 *      Author: sergeyn
 */

#ifndef BASECOMPRESS_H_
#define BASECOMPRESS_H_

#include "globals.h"

template<typename T>
class BaseCompress {
protected:
	const std::vector<T>* src;
	std::vector<unsigned char> dst;
public:
	BaseCompress(const std::vector<T>& s=std::vector<T>()) : src(&s) {}
	virtual size_t compress() = 0;
	virtual std::vector<T> decompress(char* src, size_t size,  size_t count, T jumpstart=0)=0;
	virtual size_t size() const { return dst.size(); }
	virtual ~BaseCompress() {}

	//inline size_t dumpCompressedToFile(FILE* fout) const { return fwrite(&(dst[0]), sizeof(unsigned char), dst.size(), fout); }

	virtual inline size_t dumpCompressedToFile(std::ostream& s) {
		s.write( (const char*)&dst[0],dst.size());
		return dst.size();
	}
};
//======================================================================================================
template <typename T>
class BaseVarByte : public BaseCompress<T> {
protected:
	inline void pushNumber(T n, const unsigned char mask=0) {
		if(n<128)
			BaseCompress<T>::dst.push_back(mask | (n%128) );
		else {
			pushNumber(n/128,128);
			BaseCompress<T>::dst.push_back(mask | (n%128));
		}
	}
public:
	BaseVarByte(const std::vector<T>& s) : BaseCompress<T>(s) {
		BaseCompress<T>::dst.reserve(s.size()/sizeof(T)*2);
	}
	virtual ~BaseVarByte() {}
	virtual size_t compress() = 0;
	virtual std::vector<T> decompress(char* src, size_t size, size_t count, T jumpstart=0){return {};}
	virtual std::vector<T> unpack(unsigned char* src, size_t size)const {
		std::vector<T> ret;
		ret.reserve(size);
		unsigned char mask = 127;
		T base = 0;
		for(size_t i=0; i<size; ++i) {
			base = (base << 7)+(mask&src[i]);
			if(src[i]<128) {
				ret.push_back(base);
				base = 0;
			}
		}
		return ret;
	}
};
//======================================================================================================
class VarByteForBignum : public BaseVarByte<bignum>  {
public:
	explicit VarByteForBignum(const std::vector<bignum>& s= std::vector<bignum>()) : BaseVarByte<bignum>(s) {}
	~VarByteForBignum() {}
	virtual size_t compress();
	inline virtual std::vector<bignum> decompress(char* src,size_t size,  size_t count, bignum jumpstart=0) const {
		std::vector<bignum> deltas = unpack((unsigned char*)src,size);
		deltas[0]+=jumpstart;
		for(auto i=deltas.begin()+1; i!=deltas.end(); ++i)
			*i = *(i-1)+(*i)+1;
		if(deltas.size()!=count) {
			#ifdef DEBUG
			std::cout << "GOTCHA: " << deltas.size() << " instead of " << count << " after unpacked " << size << std::endl;
			#endif
			assert(deltas.size()>count); //TODO: what is going on here?
		}

		return deltas;
	}
	virtual void test() const;
};
//======================================================================================================
class VarByteForFreqs : public BaseVarByte<unsigned int>  {
public:
	VarByteForFreqs(const std::vector<unsigned int>& s) : BaseVarByte<unsigned int>(s) {}
	virtual size_t  compress();
	virtual std::vector<unsigned int> decompress(char* src,size_t size, size_t count) const;
};
//======================================================================================================
class CompressTweetFreqs : public VarByteForFreqs  {
public:
	CompressTweetFreqs(const std::vector<unsigned int>& s) : VarByteForFreqs(s) {}
	virtual size_t compress();
	inline virtual std::vector<unsigned int> decompress(char* src, size_t size, size_t count) const{
		std::vector<unsigned int> ret(count);
		std::fill(ret.begin(),ret.end(),1);
		std::vector<unsigned int> exceptions = unpack((unsigned char*)src,size);
		assert(exceptions.size()%2 == 0);
		for(auto i = exceptions.begin(); i!=exceptions.end(); ++i) {
			const unsigned int what = *i;
			++i; //the next is where -- they come in pairs
			const unsigned int where = *i;
			assert(where < count);
			ret[where]=what+1; //note the +1
		}
		return ret;
	}

};
//======================================================================================================
class BlockedVarByteForBignum : public VarByteForBignum  {
public:
	typedef std::vector<unsigned int> blockSizesType;
	typedef std::vector<bignum> blockMinsType;
protected:
	unsigned int blockSize;
	blockSizesType compressedBlockSizes;
	blockSizesType blockOffsets; //used only for decompression
	blockMinsType blockMinDid;

public:
	BlockedVarByteForBignum(const unsigned int blockSz=64): blockSize(blockSz) {}
	BlockedVarByteForBignum(const std::vector<bignum>& s, const unsigned int blockSz=64) : VarByteForBignum(s), blockSize(blockSz) {}
	BlockedVarByteForBignum(const blockSizesType& cBSz, const blockMinsType& bMD, const unsigned int blockSz=64) :
		 	 	 	 	 	 blockSize(blockSz), compressedBlockSizes(cBSz), blockMinDid(bMD) {}

	virtual size_t dumpCompressedToFile(std::ostream& s);
	virtual size_t compress();

	void getFromStream(std::istream& s, size_t sizeCompressed, size_t blocksCount);

	inline void assertSize() const {
		size_t sz = blockOffsets.size();
		assert(sz);
	}
	size_t _sizeM() const { return sizeof(blockMinDid[0])*blockMinDid.size(); }
	size_t _sizeS() const { return sizeof(compressedBlockSizes[0])*compressedBlockSizes.size(); }
	virtual size_t size() const { return dst.size() + _sizeM() + _sizeS(); }
	void prepareForDecompression(); //this should be added to copy ctor!

	inline std::vector<bignum> getDecompressedBlock(unsigned int i) {
		assertSize();
		char* ptr = (char*)&(dst[blockOffsets[i]]);
		return VarByteForBignum::decompress(ptr,compressedBlockSizes[i],blockSize, blockMinDid[i]);
	}

	friend class CompressedBlockedList;
	virtual void test() const;
};
#endif /* BASECOMPRESS_H_ */
