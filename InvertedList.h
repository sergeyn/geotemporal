/*
 * InvertedList.h
 *
 *  Created on: Mar 13, 2012
 *      Author: sergeyn
 */

#ifndef INVERTEDLIST_H_
#define INVERTEDLIST_H_

#include "globals.h"
#include "BaseCompress.h"
#include "Tuples.h"
#include "profiling.h"

class InvListsUtils {
public:
	template<typename T>
	inline static T& getShortest(std::vector<T>& lists) {
		return *std::min_element(lists.begin(),lists.end(),[](T& a, T& b){ return a.size() < b.size(); });
	}
	template<typename T>
	inline static void sortByDid(std::vector<T>& lists) {
		std::sort(lists.begin(), lists.end(), [](const T& a, const T& b){ return a.did() < b.did(); } );
	}
	template<typename T>
	inline static void sortByLength(std::vector<T>& lists) {
		std::sort(lists.begin(), lists.end(), [](const T& a, const T& b){ return a.size() < b.size(); } );
	}
};

class InvertedList {
public:
	std::vector<bignum> ids;
	std::vector<unsigned int> freqs;
	unsigned int index;

	InvertedList() :index(0){}
	inline bignum nextGEQ() { return nextGEQ(ids[index]+1); }
	inline bignum nextGEQ(bignum docid) {
		const size_t sz = ids.size();
		while(ids[index]<docid && index<sz)
			++index;
		return ids[index];
	}
	inline bignum did() const { return ids[index]; }
	inline unsigned int freq() const { return freqs[index]; }
	inline size_t size() const { return ids.size(); }
	size_t readFromStreamAndDecompress(std::istream& strm, const lexTuple& meta);
};


class CompressedBlockedList {
	BlockedVarByteForBignum blocksCompressor;
	unsigned int currentBlock;
	unsigned int currentIndex;
	std::vector<bignum> decompressed;
	std::vector<unsigned int> freqs;
	bignum cdid;
	bignum MAXD;
public:
	CompressedBlockedList() : currentBlock(0), currentIndex(0){}
	CompressedBlockedList(const BlockedVarByteForBignum::blockSizesType& cBSz,
						  const BlockedVarByteForBignum::blockMinsType& bMD,
						  const unsigned int blockSz=64) : blocksCompressor(cBSz,bMD,blockSz), currentBlock(0), currentIndex(0)  {}

	size_t readFromStreamAndDecompress(std::istream& strm, const lexTuple& meta, bignum maxD);

	inline bignum nextGEQ(bignum docid) {
		profilerC& p = profilerC::getTheInstance();
		p.stepCounter(0);
		if(docid<=cdid)
			return cdid;

		const BlockedVarByteForBignum::blockMinsType& mindid = blocksCompressor.blockMinDid;
		
		if(currentBlock>=mindid.size()-1)
			return cdid = MAXD;
			
		unsigned int block = currentBlock;
		for(; (block < mindid.size() && docid > mindid[block]); ++block) {
		}
		skipToBlock(--block);
		
		while(currentIndex<blocksCompressor.blockSize-1 && decompressed[currentIndex]<docid )
			++currentIndex;

		bignum ret = cdid = decompressed[currentIndex];
		if(currentIndex>=blocksCompressor.blockSize-1 && block+1 < mindid.size()) {
			skipToBlock(block+1);
			if(ret>=docid)
				cdid = ret; //restore prev.
			else
				ret = cdid;
			if(currentBlock >= mindid.size()-1) //bubu?
				cdid = MAXD;
		}
		else if(currentIndex>=blocksCompressor.blockSize-1 && block+1 >= mindid.size())
			return MAXD;

		assert(ret>=docid);
		return ret;
	}

	inline bignum _skip(unsigned int block) {
		currentBlock = block;
		currentIndex = 0;
		decompressed.clear();
		decompressed = blocksCompressor.getDecompressedBlock(currentBlock);

		assert(decompressed.size() >= blocksCompressor.blockSize);
			//std::cout << decompressed.size() << " on getting block " << block <<std::endl;

		cdid = decompressed[currentIndex];
		return cdid;
	}

	inline bignum skipToBlock(unsigned int block) {
		if(currentBlock < block)
			return _skip(block);

		return cdid;
	}

	inline bignum peepNextBlock() const { return blocksCompressor.blockMinDid[currentBlock+1];  }
	inline bignum did() const { return cdid;  }
	inline unsigned int freq() const {
		return freqs[currentIndex];
	}
	inline size_t size() const { return freqs.size(); }
};

std::ostream& operator<< (std::ostream& s, const InvertedList& t);
#endif /* INVERTEDLIST_H_ */
