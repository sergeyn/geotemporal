/*
 * Geo.h
 *
 *  Created on: Apr 15, 2012
 *      Author: sergeyn
 */

#ifndef GEO_H_
#define GEO_H_

#include "globals.h"

class gPoint {
public:
	float x_crd;
	float y_crd;

	gPoint(float x=0.0, float y=0.0) : x_crd(x), y_crd(y) {}
	bool isnull() { return x_crd == 0.0 && y_crd == 0.0; }
};
std::ostream& operator<< (std::ostream& s, const gPoint& t);
//currently range is a rectangle given by north-west and south-east coordinates
//make sure that both y and x in fact grow when going gtom first to second
class gRange {
public:
	gPoint first;
	gPoint second;
	enum intersectStatus { OUTSIDE, INSIDE, PARTIAL };
	gRange(const gPoint& f=gPoint(), const gPoint& s=gPoint()) : first(f), second(s) {}
	inline bool isPointInRange(const gPoint& p) const { return p.x_crd >= first.x_crd && p.x_crd <= second.x_crd && p.y_crd >= first.y_crd && p.y_crd <= second.y_crd; }
	intersectStatus howIntersects(const gRange& r) const;
};
std::ostream& operator<< (std::ostream& s, const gRange& r);

class tRange {
public:
	typedef unsigned long long int timeType; //breaks unless 64bit
	timeType left;
	timeType right;
	tRange(timeType l=0, timeType r=0) : left(l), right(r) { assert(l<=r); }
	bool isTimeInRange(timeType t) const { return t >= left && t<=right; }
};

const gPoint SALTLAKE(-112.30523,40.8018);
const gRange USABBOX(gPoint(-124.7625, 24.5210),gPoint(-66.9326, 49.3845));
const gRange NY(gPoint(-76.7625, 36.5210),gPoint(-70.9326, 43.3845));
const tRange ALLTIME(1320700000,9320700000);

class Geo {

};

#endif /* GEO_H_ */
