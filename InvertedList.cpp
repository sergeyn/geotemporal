/*
 * InvertedList.cpp
 *
 *  Created on: Mar 13, 2012
 *      Author: sergeyn
 */

#include "InvertedList.h"
#include <limits>

std::ostream& operator<< (std::ostream& s, const InvertedList& t) {
	s << t.ids << t.freqs;
	return s;
}

template <typename DocCompress,typename FreqCompress, typename Ids, typename Freqs>
size_t _readFromStreamAndDecompress(std::istream& strm, const lexTuple& meta, Ids& ids, Freqs& freqs) {
	std::vector<unsigned char> buf(std::max(meta.sizeDocs,meta.sizeFreqs));
	char* ptr = (char*) &buf[0];
	strm.seekg(meta.address, std::ios_base::beg);
	strm.read(ptr,meta.sizeDocs);

	DocCompress cmpb({});
	ids = cmpb.decompress(ptr,meta.sizeDocs,meta.count);

	strm.read(ptr,meta.sizeFreqs);
	FreqCompress cmp({});
	freqs = cmp.decompress(ptr,meta.sizeFreqs, meta.count);
	if(ids.size() != freqs.size()) {
		std::cout << meta << " => " << ids.size()  << " " << freqs.size() << std::endl;
		THROW("meta err");
	}
	ids.push_back(std::numeric_limits<unsigned int>::max());
	return freqs.size();
}

size_t InvertedList::readFromStreamAndDecompress(std::istream& strm, const lexTuple& meta) {
	return _readFromStreamAndDecompress<COMPRESSTYPES, std::vector<bignum>, std::vector<unsigned int> >(strm,meta,ids,freqs);
}


size_t CompressedBlockedList::readFromStreamAndDecompress(std::istream& strm, const lexTuple& meta, bignum maxD) {
	MAXD = maxD;
	strm.seekg(meta.address, std::ios_base::beg);
	const unsigned int blocksCount = meta.count%blocksCompressor.blockSize ?
			1+(meta.count/blocksCompressor.blockSize) : meta.count/blocksCompressor.blockSize;
	assert(blocksCount);
	assert(meta.sizeDocs);
	blocksCompressor.getFromStream(strm,meta.sizeDocs,blocksCount);

	std::vector<unsigned char> buf(meta.sizeFreqs);
	char* ptr = (char*) &buf[0];
	strm.read(ptr,meta.sizeFreqs);
	CompressTweetFreqs cmp({});
	freqs = cmp.decompress(ptr,meta.sizeFreqs, meta.count);
	assert(freqs.size() == meta.count);
	blocksCompressor.assertSize();
	_skip(0);
	return freqs.size();
}

