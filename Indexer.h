/*
 * Indexer.h
 *
 *  Created on: Mar 11, 2012
 *      Author: sergeyn
 */

#ifndef INDEXER_H_
#define INDEXER_H_

#include <vector>
#include <unordered_map>
#include <unordered_set>

#include "globals.h"
#include "RawDocument.h"
#include "Merger.h"
#include "Tuples.h"


class CompressAndDump {
	lexicon& lex;
	const std::string fname;
	std::ofstream binary;
	std::ofstream asci;
	std::ofstream lexic;
	std::ofstream docsMetaStream;
	bignum binaryBsWritten;

public:
	CompressAndDump(lexicon& l, const std::string f)
	  : lex(l), fname(f), binary(f+".bin", std::ios::binary), asci(f+".asc"), lexic(f+".lex"),docsMetaStream(f+".len"), binaryBsWritten(0)	{}

	template <typename DocCompress,typename FreqCompress>
	lexTuple compress(const std::vector<bignum>& ids, const std::vector<unsigned int>& freqs) {
		DocCompress cmpDids(ids);
		cmpDids.compress();
		FreqCompress cmpFs(freqs);
		cmpFs.compress();
		cmpDids.dumpCompressedToFile(binary);
		cmpFs.dumpCompressedToFile(binary);
		return lexTuple("",binaryBsWritten,cmpDids.size(),cmpFs.size(),ids.size());
	}

	void operator()(const std::vector<termTuple>& oneTerm);
	void operator()();
	void operator()(std::vector<docTuple>& docsMeta);
};

class Indexer {
	std::vector<std::string> inputFiles;
	std::string workdir;

	//DocIdMapper mapper;
	lexicon lex;

	void processRedundFile(const std::string& f, absDocIdMapper& mapper);
	std::mutex sharedMutex;

	unsigned int docsCount;

public:
	Indexer(const std::vector<std::string>& inf, const std::string& wd) :
		inputFiles(inf), workdir(wd) /*mapper(sharedMutex)*/{}
	Indexer(std::vector<docTuple>::iterator begin, std::vector<docTuple>::iterator end);
	Indexer(std::vector<docTuple>& inputForSingle);
	void operator()();
	size_t size() const { return docsCount; }
};

#endif /* INDEXER_H_ */
