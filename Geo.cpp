/*
 * Geo.cpp
 *
 *  Created on: Apr 15, 2012
 *      Author: sergeyn
 */

#include "Geo.h"

gRange::intersectStatus gRange::howIntersects(const gRange& r) const {
	if( isPointInRange(r.first) && isPointInRange(r.second) )
		return INSIDE;
	if( ! (isPointInRange(r.first) || isPointInRange(r.second)) )
		return OUTSIDE;
	return PARTIAL;
}

std::ostream& operator<< (std::ostream& s, const gPoint& t) {
	s.precision(8);
	s << t.x_crd << " " << t.y_crd;
	return s;
}

std::ostream& operator<< (std::ostream& s, const gRange& r) {
	s << r.first << " " << r.second;
	return s;
}
