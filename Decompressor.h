/*
 * Decompressor.h
 *
 *  Created on: Mar 16, 2012
 *      Author: sergeyn
 */

#ifndef DECOMPRESSOR_H_
#define DECOMPRESSOR_H_

#include <zlib.h>
#include "globals.h"

//read the gzip file into memory
class Decompressor {
protected:
	std::string name;
	char buffer[1024];
public:

	Decompressor(std::string fname="") : name(fname) {}

	class CantOpen : public std::exception {};

	void decompress(std::stringstream& rawdata) { //read the gzip file into memory
	    gzFile infile = gzopen((name).c_str(), "rb");
	    if (!infile) {std::cerr << name << " can't open" << std::endl; abort(); }
	    int num_read = 0;
	    while ((num_read = gzread(infile, buffer, sizeof(buffer))) > 0)
		rawdata.write(buffer,num_read);
	    gzclose(infile);
	}
};

class SmalGzipStreamer : public StringStreamer {
public:
	SmalGzipStreamer(const std::string& f) : StringStreamer(f) {
			handle.clear();
			Decompressor dc(f);
			dc.decompress(handle);
			handle.seekg(0,std::ios_base::seekdir::_S_beg);
	}
};

#endif /* DECOMPRESSOR_H_ */
