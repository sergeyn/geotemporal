/*
 * BaseCompress.cpp
 *
 *  Created on: Mar 13, 2012
 *      Author: sergeyn
 */

#include "BaseCompress.h"
#include <limits>

 size_t VarByteForBignum::compress() { //calculateGapsAndCompressForDocIDs
		pushNumber((*src)[0]);
		for(size_t i=1; i<src->size(); ++i)
				pushNumber((*src)[i]-(*src)[i-1]-1);
		return dst.size();
	}

	void VarByteForBignum::test() const {
		std::vector<bignum> s;
		for(unsigned int i=0; i<1234567; ++i)
			s.push_back(rand());
		std::sort(s.begin(),s.end());
		VarByteForBignum c(s);
		size_t size = c.compress();
		char* ptr = (char*)&(c.dst[0]);
		auto d = c.decompress(ptr,size,s.size());
		std::cout << "test: " << std::equal(s.begin(), s.end(), d.begin()) << std::endl;

	}

	size_t  VarByteForFreqs::compress()  {
		for(auto it = src->begin(); it!=src->end(); ++it)
				pushNumber((*it)-1);
		return dst.size();
	}

	std::vector<unsigned int> VarByteForFreqs::decompress(char* src,size_t size, size_t count) const {
	        std::vector<bignum> deltas = unpack((unsigned char*)src,size);
                std::vector<unsigned int> ret;
                ret.reserve(deltas.size());
                for(bignum& i : deltas)
                        i = i+1;
                return deltas;
	}

	size_t CompressTweetFreqs::compress()  { //Simply record the indices where freq>1
		size_t i=0;
		for(auto it = src->begin(); it!=src->end(); ++it, ++i)
			if((*it) > 1) {
				pushNumber((*it)-1); //what - note the -1
				pushNumber(i); //where
			}
		return dst.size();
	}


	size_t BlockedVarByteForBignum::compress()  {
		size_t i=0;
		compressedBlockSizes.reserve(src->size()/blockSize);
		size_t offset = 0;

		pushNumber(0); //always put 0 -- we will figure this out from block mins
		blockMinDid.push_back((*src)[i]); //put the uncompressed
		for(i=1; i<src->size(); ++i) {
			if(i%blockSize == 0) {
				compressedBlockSizes.push_back(dst.size()-offset);
				offset = dst.size();
				pushNumber(0); //always put 0 -- we will figure this out from block mins
				blockMinDid.push_back((*src)[i]); //put the uncompressed
			}
			else
				pushNumber((*src)[i]-(*src)[i-1]-1); //put the gap-1
		}
		pushNumber(std::numeric_limits<unsigned int>::max()-(*src)[i-1]-1);
		while((i+1)%blockSize) {
			++i;
			pushNumber(0); //padding -- perhaps maxd?
	}
		compressedBlockSizes.push_back(dst.size()-offset);
		offset = dst.size();

		return BlockedVarByteForBignum::size();
	}

	void BlockedVarByteForBignum::prepareForDecompression() {
		if(blockOffsets.size())
			return;
		size_t sz = compressedBlockSizes.size();
		assert(sz);
		blockOffsets.reserve(1+compressedBlockSizes.size());
		#ifdef DEBUG
			blockOffsets.resize(1+compressedBlockSizes.size());
		#endif
		blockOffsets[0]=0;
		std::partial_sum(compressedBlockSizes.begin(), compressedBlockSizes.end(),blockOffsets.begin()+1);
	}

	size_t BlockedVarByteForBignum::dumpCompressedToFile(std::ostream& s) {
		// serialization scheme:
		// we should dump both vectors: compressedBlockSizes and blockMinDid with the docid dump
		BaseCompress::dumpCompressedToFile(s);
		s.write( (const char*)&compressedBlockSizes[0],_sizeS());
		s.write( (const char*)&blockMinDid[0],_sizeM());
		return BlockedVarByteForBignum::size();
	}

	void BlockedVarByteForBignum::getFromStream(std::istream& s, size_t sizeCompressed, size_t blocksCount) {
		const unsigned int sizesSz = blocksCount*sizeof(compressedBlockSizes[0]);
		const unsigned int sizesMins = blocksCount*sizeof(blockMinDid[0]);
		const unsigned int sizesDids = sizeCompressed-sizesMins-sizesSz;
		dst.resize(sizesDids);
		s.read((char*)&(dst[0]),sizesDids);
		compressedBlockSizes.resize(blocksCount);
		blockMinDid.resize(blocksCount);
		s.read((char*)&compressedBlockSizes[0],sizesSz);
		s.read((char*)&blockMinDid[0],sizesMins);
		prepareForDecompression();
	}

	void BlockedVarByteForBignum::test() const {
		std::vector<bignum> s;
		for(unsigned int i=0; i<1234567; ++i)
			s.push_back(rand());
		std::sort(s.begin(),s.end());
		BlockedVarByteForBignum c(s);
		c.compress();
		//now decompress block by block
		c.prepareForDecompression();
		for(size_t i=0; i<c.compressedBlockSizes.size(); ++i) {
			auto d = c.getDecompressedBlock(i);
			if(! std::equal(s.begin()+(i*blockSize), s.begin()+((1+i)*blockSize), d.begin()) ) {
					std::cout << "block " << i << std::endl;
					std::cout << d << std::endl;
					for(auto dd = s.cbegin()+(i*blockSize); dd!=s.cbegin()+((1+i)*blockSize); ++dd)
					std::cout << *dd << " " ;
					std::cout << std::endl;
					return;
			}
		}
		std::cout << "passed test " << __FILE__ << ":" << __LINE__ << std::endl;
	}
