#include <iostream>
#include <string> 
#include <map> 
#include <vector>
#include <algorithm>

typedef std::pair<std::string, int> histDatum;

bool order(histDatum x, histDatum y) 
{
	return ( x.second > y.second ); 
}

int _main(int argc, char const *argv[])
{
	int K = atoi(argv[1]); 

	std::map <std::string, int> termHist; 

	std::string word;
	
	while (std::cin >> word && word[0])
	{
		if (termHist.count(word) == 0)
			termHist[word] = 1; 
		else 
			termHist[word] += 1;
	}
	
	std::map<std::string, int>::iterator it = termHist.begin(); 
	std::vector<histDatum> sortedHist; 

	for (int i = 0; it != termHist.end(); i++)
	{
		sortedHist.push_back(histDatum(it->first, it->second)); 
		it++; 
	}
	
	if (K > termHist.size())
		K = termHist.size(); 
	
	sort(sortedHist.begin(), sortedHist.end(), order); 

	for (int i = 0; i < K; i++)
	{
		std::cout << sortedHist[i].first << " " << sortedHist[i].second << std::endl;
	}
	return 0;
}
