require 'socket'               # Get sockets from stdlib
rawTweetsMask = ARGV[0]
mapTO = ARGV[1] 
server = TCPServer.open(5998)  # Socket to listen on port
STDERR.puts "read map to mem."
ssize = mapTO.split('mapIdOToffset')[-1].to_i
STDERR.puts ssize
arr = File.new(mapTO).read(ssize*8*2).unpack("Q#{ssize*2}")

class Twpair
include Comparable
attr_accessor :tid, :off
  def <=>(t)
     @tid <=> t.tid
  end

  def initialize(t,of)
    @tid=t
    @off=of
  end
end
STDERR.puts "..."
newarr = []
i=0
while(i<arr.size-1) 
	newarr << Twpair.new(arr[i+1],arr[i])
	i +=2
end
STDERR.puts "done. Now sorting..."
newarr.sort!

def binarySearch(sortedArray,key,first=0,last=sortedArray.length-1)
	return -1 unless first<=last

	mid = (first + last)/2
	return mid if (key == sortedArray[mid])
  
	if(key > sortedArray[mid]) 
		first = mid + 1 
	else 
		last = mid - 1 
	end 
	return binarySearch(sortedArray, key,first, last) 
end

STDERR.puts "Server is up"

def genHist(tweets, k)

	if tweets.size() > 0 
		io = IO.popen("./hist #{k}", "r+")

		tweets.each do |tweet|
			#Need only text
			terms = tweet.split(" ")[5..-1]
			terms.each do |term|
				io.puts term.downcase
			end 
		end

		io.puts "\0" 

		result = io.readlines("\n");

		io.close()

		return result 
	end 

	return ""
	
end

loop {                         # Servers run forever
    client = server.accept       # Wait for a client to connect
    req = client.gets
	f = File.new(rawTweetsMask)
	STDERR.puts req

	tweetArr = []
	reqParam = req.split('?')[-1].split(',')

	k = reqParam[0].to_i
	tweetIds = reqParam[1..-1]

	tweetIds.each { |tid| 
		key = binarySearch newarr,Twpair.new(tid.chomp.to_i,0)
		#puts "tid: #{tid.chomp.to_i}, key: #{key}." 
		if(key>-1) 
			f.seek(newarr[key].off,IO::SEEK_SET)
			tweetArr << f.readline

			#client.puts(f.readline)
		end
	}

	tweetStr = tweetArr.join("\n")

	if k == 0
		client.puts(tweetStr)
	elsif k > 0  
		client.puts(genHist(tweetArr, k))
	end

  client.close                 # Disconnect from the client
}

