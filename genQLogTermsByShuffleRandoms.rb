def getTermsArr
	%x[./genQsByDiscreteFreq].split "\n"
end

if ARGV.size < 2
	puts "usage: script k-terms-query #count-of-queries"
	exit
end

arr = []
k = ARGV[0].to_i
countT = ARGV[1].to_i

while(arr.size < k*countT)
	arr.concat getTermsArr
	STDERR.puts arr.size
end

i = 0
countT.times {
  q = arr[i...i+k].uniq
	puts q.join ' ' if q.size == k
	i+=k
}
	

