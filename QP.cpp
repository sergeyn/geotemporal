/*
 * QP.cpp
 *
 *  Created on: Mar 14, 2012
 *      Author: sergeyn
 */

#include "QP.h"

	size_t QP::globalMAXD(0);

	bignum magicFormula(tRange::timeType t) {
		//const double ml = 7.7877581473;
		//const double subs = 85421.23;
		const double ml = 9.249013709;
		const double subs = 1191641.947;

		double fx = (double(t)*ml)-subs; //if smallset is used

		if(fx<0.0) return 0;
		
		return std::min((long int)(QP::globalMAXD-256),lrint(fx));
	}

void QP::calculateSafeMargins() {
	safetyMarginLeft = std::numeric_limits<bignum>::max();
	safetyMarginRight = 0;
	for(auto d : doclen.hashmapping) {
		int delta =	d.second.docid - magicFormula(d.second.seconds);
		if(delta<safetyMarginLeft)
			safetyMarginLeft = delta;
		if(delta>safetyMarginRight)
			safetyMarginRight=delta;
	}
	std::cout << "Setting margins: " << safetyMarginLeft << "," << safetyMarginRight << std::endl;
}

bignum QP::getMaxD(tRange::timeType right) {
	return magicFormula(right)+safetyMarginRight;
}


bignum QP::getStartD(tRange::timeType left) {
	return std::max(int(1),int(magicFormula(left))+safetyMarginLeft);
}

