/*
 * QP.h
 *
 *  Created on: Mar 14, 2012
 *      Author: sergeyn
 */

#ifndef QP_H_
#define QP_H_

//#include <limits>
#include "globals.h"
#include "InvertedList.h"
#include "Tuples.h"


template<class T>
class PriorityArray{
	const size_t size;
	bool flagIsHeap;
    std::vector<T> minData;
    std::vector<T> vData;

public:
    PriorityArray(const size_t sz):size(sz){
    	flagIsHeap = false;
  //  	minData.resize( std::min(size_t(128),size+1));
  //  	std::fill(minData.begin(),minData.end(),std::numeric_limits<T>::min());
    }

    const std::vector<T>& getV() const { return (flagIsHeap) ? minData : vData;}

   inline bool push(const T& val){
    	if( vData.size() < size) { //still a vector
    		vData.push_back(val);
    		return true;
    	}

    	if(flagIsHeap)
    		return pushHeap(val);

    	//only now we are a heap -- need to optimize this one
    	flagIsHeap = true;
    	minData.resize(size+1);
    	std::fill(minData.begin(),minData.end(),std::numeric_limits<T>::min());
    	for(T& v : vData)
    		pushHeap(v);
    	return pushHeap(val);
    }

    inline bool pushHeap(const T& val) {
    	//are we better than the smallest top-k member?
    	if(minData[0]>val)
    		return false;
    	minData[0] = val;

		for (unsigned int i = 0; (2*i+1) < minData.size(); ){
			unsigned int next = 2*i + 1;
			if (minData[next] > minData[next+1])
				++next;
			if (minData[i] > minData[next])
				std::swap(minData[i], minData[next]);
			else
				break;
			i = next;
		}
		return true;
    }
};

bignum magicFormula(tRange::timeType t);

class QP {
	lexicon lex;
	doclenMap doclen;
	std::ifstream bin;
	unsigned int topK;
	bignum MAXD;
	bignum startD;

	int safetyMarginLeft;
	int safetyMarginRight;

	void calculateSafeMargins();

public:
	static size_t globalMAXD;

	QP(std::string indexPath, unsigned int tK) : lex(indexPath+".lex"), doclen(indexPath+".len"), bin(indexPath + ".bin", std::ios::binary), topK(tK) {
		std::cout << "Loaded lexicon: " << lex.size() << " terms" << std::endl;
		calculateSafeMargins();
	}

	bignum getMaxD(tRange::timeType right);
	bignum getStartD(tRange::timeType left);
/*
	void append(const docTuple& doc, const std::vector<std::string>& terms);
	template<typename Parser>
	void append(const std::string& chunk) {
		docTuple meta;
		meta.docid = ++MAXD;
		Parser p;
		append(meta,p(chunk, meta));
	}*/

	void setStartEndDs(bignum s, bignum e) {MAXD=e; startD=s;}  //setting docs range for next query
	const docTuple& getDocMeta(bignum did)  { return doclen.getDocMeta(did); } //called only from qp filter results!
	std::string getUrl(bignum did) const { return doclen[did].url; }
	gPoint getLocation(bignum did) const { return doclen[did].location; }
	tRange::timeType getTime(bignum did) const { return doclen[did].seconds; }

	inline float _rankWithBM25(bignum did, unsigned int f, unsigned int lengthOfList, const queryTuple& query) {
	        float idf;
	        const docTuple& meta = doclen.getDocMeta(did);
            unsigned int dlen = 0; // meta.size;
            if(! (query.geoR.isPointInRange(meta.location) && query.timeR.isTimeInRange(meta.seconds)))
            	return 0.0;
	        idf = log( 1+(float)(MAXD - lengthOfList + 0.5)/(float)(lengthOfList + 0.5))/log(2) ;
	        return (idf * ( (float)(f * 3)/(float)(f + 2*( 0.25 + 0.75 * (float)(dlen)/(float)(130) ) ) ));
	}

	template<typename IList>
	inline float rankWithBM25(const std::vector<IList>& lists, const queryTuple& query) {
		float s=0.0;
		for(const IList& f : lists)
			s+=_rankWithBM25(f.did(),f.freq(),f.size(),query);

		return s;
	}

	template<typename IList>
	results _single (std::vector<IList>& lists, const queryTuple& query) {
			return results();
		if(lists.empty())
			return results();
		PriorityArray<result> topkHeap(topK);
		bignum pivotId(startD);
		IList& list = lists[0];
		while( (pivotId = list.nextGEQ(pivotId+1)) < MAXD) {
			result r = result(pivotId, rankWithBM25(lists,query));
			topkHeap.push(r);
		}
		return topkHeap.getV();
	}

	template<typename IList>
	results andQuery(std::vector<IList>& lists, const queryTuple& query)	{
		profilerC& p = profilerC::getTheInstance();
		p.start(5);
		if(lists.size()<2)
			return _single(lists,query); //note: works fine with single list, just an optimization...

		InvListsUtils::sortByLength(lists);
		IList& shortest = lists[0];

		PriorityArray<result> topkHeap(topK);
		bignum pivotId(startD);
		p.end(5);
		while( (pivotId = shortest.nextGEQ(pivotId)) < MAXD) {

			auto it = lists.begin()+1;
			for(; it != lists.end() && (pivotId == (*it).nextGEQ(pivotId)); ++it){
			}

			if(it != lists.end())
				pivotId = (*it).did();  //skip to the guy that failed us
			else { //intersection!
				p.stepCounter(1);
				float score = rankWithBM25(lists,query);
				result r=result(pivotId,score);
				topkHeap.push(r); //std::cout << "hit: " << pivotId <<  " score: " << score  << std::endl;
				++pivotId;
			}
		}
		return topkHeap.getV();
	}

	template<typename IList>
	void openlists(const std::vector<std::string>& terms, std::vector<IList>& lists) {
			//open lists:
			std::vector<lexTuple> lexs;
			lexs.reserve(terms.size());
			for(const std::string& i : terms) {
				if(lex.end()!=lex.find(i))
					lexs.push_back(lex[i]);
				#ifdef DEBUG
				else
					std::cerr << i << " not in lexicon - omitting" << std::endl;
				#endif

			}
			if(lexs.empty())
				return;

			lists.resize(lexs.size());
			for(unsigned int i=0; i<lexs.size(); ++i)
				lists[i].readFromStreamAndDecompress(bin,lexs[i],MAXD);
			return;
	}
};

#endif /* QP_H_ */
