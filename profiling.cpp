/*
 * profiling.cpp
 *
 *  Created on: Jan 12, 2012
 *      Author: sergeyn
 */
#include "profiling.h"
#include "globals.h"

profilerC profilerC::GlobalProfiler;
dummyProfiler dummyProfiler::dummy;

profilerC::profilerC() :cycles(0) {
	//setCosts();
	//for(unsigned i=0; i<1 ++i) {
		initNewCounter();  //initialize named counters
		add("0",0); //init named timers
	//}
}

void profilerC::printReport() const {
	std::unordered_map<int,sTimer>::const_iterator it;
	for ( it=timers.begin() ; it != timers.end(); it++ ) {
		int id = (*it).first;
		const sTimer& t = (*it).second;
		const std::string cm((*comments.find(id)).second);
		if(t.getCount()<1) {
			std::cout << "skipping timer " << cm << std::endl;
			continue;
		}

		unsigned long count = t.getCount();
		double total = 	t.getTotal()/1000.0; //in ms
		
		std::cout << id << ": "<< cm <<
		" total(ms): " << total /*-(cycleCost*(t.getCount()/100000000))*/
	 	<< /*"(" << total << */" count: " << count  <<
		" avg: " <<   total/count <<std::endl;

	}
	unsigned i = 0;
	for(unsigned long c : counters) {
		if(c>0) {std::cout << i << ": " << "name" << " count: " << c << std::endl;}
		++i;
	}
}

void thrashSomeCache(const unsigned size) {
             char *c = (char *)malloc(size);
             for (int i = 0; i < 0xffff; i++)
               for (unsigned j = 0; j < size; j++)
                 c[j] = i^j;
             free(c);
}
