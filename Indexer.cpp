/*
 * Indexer.cpp
 *
 *  Created on: Mar 11, 2012
 *      Author: sergeyn
 */

#include "Indexer.h"
#include "BaseCompress.h"
//#include <boost/filesystem.hpp>

void CompressAndDump::operator()(const std::vector<termTuple>& oneTerm) {
	if(oneTerm.size()==0)
		return;
	const std::string& term = oneTerm[0].term;

	std::vector<bignum> ids;
	std::vector<unsigned int> freqs;
	for(auto it = oneTerm.begin(); it != oneTerm.end(); ++it) {
		const termTuple& t = *it;
		ids.push_back(t.docid);
		freqs.push_back(t.freq);
	}
	#ifdef DEBUG
	asci << term << " ";
	for(auto it = ids.begin(); it!=ids.end(); ++it )
		asci <<  *it << " " ;
	for(auto it = freqs.begin(); it!=freqs.end(); ++it )
		asci <<  *it << " " ;
	asci <<  " " << std::endl;
	#endif

	lexTuple lt = compress<COMPRESSTYPES>(ids,freqs);
	lt.term = term;
	lex[term] = lt;
	binaryBsWritten += lt.sizeDocs + lt.sizeFreqs;

}

void CompressAndDump::operator()() { for(auto& p : lex) lexic << p.second; }

void CompressAndDump::operator()(std::vector<docTuple>& docsMeta) {
	docTuple::sortBySeconds(docsMeta.begin()+1, docsMeta.end()); //didn't we do it already?
	for(const docTuple& d : docsMeta)
		docsMetaStream << d << std::endl;
}


template <typename StreamT, typename ParserT>
std::vector<std::string> splitFileToWorkers(const std::unordered_set<std::string>& ids, const std::string& srcfname, const std::string& prefix, const unsigned int workers ) {
	StreamT stream(srcfname);
	ParserT parser;
	std::string chunk = "";
	std::ofstream outfiles[workers];
	std::vector<std::string> inputFiles;
	inputFiles.reserve(workers);
	for(unsigned int i=0; i<workers; ++i) {
		const std::string fname(prefix+int2s(i));
		outfiles[i].open(fname.c_str());
		inputFiles.push_back(fname);
	}

	bignum i = 0;
	if(ids.empty()) //debug feature
		return std::vector<std::string>();
	while("" != (chunk = stream.getChunk())) {
		docTuple docT;
		ParserT::getMeta(chunk,docT);
		if(ids.count(docT.url)) {
			outfiles[i%workers] << chunk << std::endl;
			++i;
		}
	}
	return inputFiles;
}

Indexer::Indexer(std::vector<docTuple>& inputForSingle) 
{
	const std::string allDocsSuffix("allDocs");
	docsCount = inputForSingle.size();
	std::string dir = "test";
	std::string srcFile(dir+"/allDocs");
	std::cout << "process: " << srcFile << " count: " << docsCount << std::endl;
	std::unordered_set<std::string> ids;
	for(std::vector<docTuple>::iterator it=inputForSingle.begin(); it!=inputForSingle.end(); ++it)
		ids.insert(it->url);

	std::vector<docTuple>& presets = inputForSingle;
	presetDocIdMapper preMapper(presets);	
	
	//ok, we got the file. now we need to create mulptiple files for multiples threads to work on
	inputFiles = splitFileToWorkers<PARSETYPES>(ids,srcFile,dir+"/work",MRGTREADSCOUNT);

	std::cout << "start concurrent preprocessing of files with " << MRGTREADSCOUNT << " threads" << std::endl;
	std::string IndexOutputFileName= dir+"/index";
	{
		Merger merger(preMapper,inputFiles,dir);
		merger.preprocess(); //perform multi-t preprocessing of the files
		merger.merge(IndexOutputFileName);
	} //let them go out of scope
	std::cout << "merge dumped all to one file: " <<IndexOutputFileName << std::endl;
	//now we have: 1) docid->size 2) large ascii file with all the terms but not aggregated
	processRedundFile(IndexOutputFileName,preMapper);
}


Indexer::Indexer(std::vector<docTuple>::iterator begin, std::vector<docTuple>::iterator end)  {
	const std::string allDocsSuffix("allDocs");
	docsCount = end-begin;
	std::string key = docTuple::createKey(begin,end);
	std::string up = "";
	const std::string dir(registry::getInstance()[key]);
	std::string srcFile(dir + "/" + allDocsSuffix );
	system(("mkdir -p "+dir).c_str());
//	boost::filesystem::create_directories(dir);
	while(!fexists(srcFile)) { //go up the hierarchy until you find an allDocs file to work on...
		up += "/../";
		srcFile = registry::getInstance()[key] + up + allDocsSuffix;
		//add a stopping condition at root...
	}
	std::cout << "process: " << srcFile << " in " << dir << " count: " << docsCount << std::endl;
	std::unordered_set<std::string> ids;
	for(std::vector<docTuple>::iterator it=begin; it!=end; ++it)
		ids.insert(it->url);

	std::vector<docTuple> presets;
	presets.push_back(docTuple()); //infamous zero tuple
	presets.resize(docsCount+1);
	std::copy(begin,end,presets.begin()+1);
	presetDocIdMapper preMapper(presets);

	//ok, we got the file. now we need to create mulptiple files for multiples threads to work on
	inputFiles = splitFileToWorkers<PARSETYPES>(ids,srcFile,dir+"/work",MRGTREADSCOUNT);

	std::cout << "start concurrent preprocessing of files with " << MRGTREADSCOUNT << " threads" << std::endl;
	std::string IndexOutputFileName;
	{
		Merger merger(preMapper,inputFiles,dir);
		merger.preprocess(); //perform multi-t preprocessing of the files

		IndexOutputFileName = dir+"/index";
		merger.merge(IndexOutputFileName);
	} //let them go out of scope
	std::cout << "merge dumped all to one file: " <<IndexOutputFileName << std::endl;
	//now we have: 1) docid->size 2) large ascii file with all the terms but not aggregated
	processRedundFile(IndexOutputFileName,preMapper);
	std::string rmCmd = "rm -f " + dir + "/work* " + dir + "/index";
	system(rmCmd.c_str());
}


void Indexer::operator()() {
	//will be deprecated with the introduction of the tree...
	THROW("DEPRECATED"); /*
	std::cout << "start concurrent preprocessing of files with " << MRGTREADSCOUNT << " threads" << std::endl;
	Merger merger(mapper,inputFiles,workdir);
	merger.preprocess(); //perform multi-t preprocessing of the files

	//Order the docids before merging
	auto oldDocids = mapper.getDocsMeta();
	docTuple::sortBySeconds(oldDocids.begin()+1,oldDocids.end());
	std::vector<bignum> oldDocIdToNew;
	oldDocIdToNew.resize(oldDocids.size());
	bignum i = 0;
	for(docTuple& dt : oldDocids) {
		oldDocIdToNew[dt.docid]=i;
		dt.docid = i;
		++i;
	}
	merger.reorderDocIds(oldDocIdToNew);
	const std::string IndexOutputFileName("index");
	merger.merge(IndexOutputFileName);
	std::cout << "merge dumped all to one file: " <<IndexOutputFileName << std::endl;
	//now we have: 1) docid->size 2) large ascii file with all the terms but not aggregated
	processRedundFile(IndexOutputFileName);
	*/
}



void Indexer::processRedundFile(const std::string& filename, absDocIdMapper& mapper) {
	AsciiStreamer strm(filename);

	CompressAndDump dump(lex, filename);
	std::string chunk;
	std::string currentTerm="";
	std::vector<termTuple> tups;

	while("" != (chunk=strm.getChunk())) {
		termTuple t = termTuple::fromString(chunk);
		if(t.term != currentTerm) { //new term!
			dump(tups); //write single inverted list
			tups.clear();
			currentTerm = t.term;
		}
		tups.push_back(t);
	} //end while

	dump(mapper.getDocsMeta()); //TODO: make this a mapper method...
	dump();
	dump(tups);  //write single inverted list (last)
}

