/*
 * Tuples.cpp
 *
 *  Created on: Mar 15, 2012
 *      Author: sergeyn
 */

#include "Tuples.h"


lexTuple lexTuple::fromString(const std::string& chunk){
	lexTuple t;
	std::istringstream iss(chunk);
	iss >> t.term >> t.count >> t.address >> t.sizeDocs >> t.sizeFreqs;
	return t;
}

std::ostream& operator<< (std::ostream& s, const lexTuple& t) {
	s << t.term << " " << t.count << " " << t.address << " " << t.sizeDocs << " " << t.sizeFreqs << std::endl;
	return s;
}

std::ostream& operator<< (std::ostream& s, const docTuple& t) {
	s << 0/*t.size*/ << " " << t.url << " " << t.seconds << " " << t.location << " " << t.docid;
	return s;
}

lexicon::lexicon(const std::string fname) {
	AsciiStreamer strm(fname);
	std::string chunk;
	while("" != (chunk = strm.getChunk())) {
		lexTuple l = lexTuple::fromString(chunk);
		this->operator [](l.term) = l;
	}

	int dummy;std::cout << "dummy stop. enter number"<<std::endl; std::cin>>dummy;
}

docTuple docTuple::fromString(const std::string& chunk) {
	docTuple t;
	std::istringstream iss(chunk);
	float x,y;
	int dummy;
	iss >> dummy /*t.size*/ >> t.url >> t.seconds >> x >> y >> t.docid;
	t.url = "";
	t.location = gPoint(x,y);
	return t;
}


doclenMap::doclenMap(const std::string fname){ //this one is only called during qp - convert vector to hash
	AsciiStreamer strm(fname);
	std::string chunk;
	while("" != (chunk = strm.getChunk())) {
		docTuple d = docTuple::fromString(chunk);
		hashmapping[d.docid]=d;
		//push_back(d);
	}
	//hashmapping.reserve(size());
	//for(auto it = begin(); it!=end(); ++it)

	std::cout << fname << " produced " << hashmapping.size() << " pairs" << std::endl;
	clear(); //remove data from vector
	resize(0);
	std::cout << hashmapping.size() << "*" << sizeof(docTuple) << " = " << hashmapping.size() * sizeof(docTuple) << std::endl;
	int dummy;std::cout << "dummy stop. enter number"<<std::endl; std::cin>>dummy;

}

termTuple termTuple::fromString(const std::string& chunk) {
	termTuple t;
	std::istringstream iss(chunk);
	iss >> t.term >> t.docid >> t.freq;
	return t;
}

std::ostream& operator<< (std::ostream& s, const termTuple& t) {
	s << t.term  << " " <<  t.docid << " " << t.freq << std::endl;
	return s;
}

inline char webFilterChar(char c) {
	return (c == '&' || c == '=') ? ' ' : c;
}

queryTuple queryTuple::fromWebString(std::string& s) {
	//x=4&y=5&z=6&w=7&s=8&e=9&keywords=1,2,3
	std::transform(s.begin(), s.end(), s.begin(), webFilterChar);
	std::string key;
	queryTuple t;
	std::istringstream iss(s);
	iss >> key >> t.geoR.first.x_crd >> key >> t.geoR.first.y_crd >> key >>  t.geoR.second.x_crd >> key >> t.geoR.second.y_crd
		>> key >> t.timeR.left >> key >>  t.timeR.right >> key >> key;
	t.terms = tokenize(key,",");
	return t;
}

queryTuple queryTuple::fromString(const std::string& chunk) {
	std::istringstream iss(chunk);
	queryTuple t;
	iss >> t.geoR.first.x_crd >> t.geoR.first.y_crd >> t.geoR.second.x_crd >> t.geoR.second.y_crd
	>> t.timeR.left >> t.timeR.right;
	std::vector<std::string> tmp = tokenize(iss.str());
	t.terms.resize(tmp.size()-6);
	std::copy(tmp.begin()+6, tmp.end(),t.terms.begin());
	return t;
}

std::ostream& operator<< (std::ostream& s, const queryTuple& t) {
	s.precision(8);
	s << t.geoR.first.x_crd << " " << t.geoR.first.y_crd << " " << t.geoR.second.x_crd <<" " << t.geoR.second.y_crd
	  << " " <<  t.timeR.left << " " << t.timeR.right << " " << t.terms;
	return s;
}

std::ostream& operator<< (std::ostream& s, const result& t) {
	s.precision(8);
	s <<"# " << t.location.y_crd << " " << t.location.x_crd << " " << t.timestamp << " " << t.score << " " << t.did << " " << t.url << std::endl;
	return s;
}
