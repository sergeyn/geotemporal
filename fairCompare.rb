#
if ARGV.size < 2
  puts "Usage: <this> single.results tree.results"
  exit
end

#$$ -124.7625 24.521 -66.932602 49.384499 1 3265157 fcfdfjfhhffhfhhffhhfhjjhhfhfhfhfhrhd @randolphacademy of @j263doe  | intersected 16 nodes and had 1  results in time: 0.029
def loadRes fname
  res = []
  File.new(fname).each { |l| 
     a = l.split()
     res << [ a[-1].to_f, a[-5].to_i] if l.include? '$$'
  }
  res
end
#--------------------------------------------------
def filterTheTwoZipped zipped
  data = []
  zipped.each_with_index { |p,i|
    data << [i,p[0][0],p[1][0]]  if p[0][1]>0 and p[1][1]>0 
  }
  data
end

single = loadRes ARGV[0]
tree = loadRes ARGV[1]

throw 'unequal' unless single.size  == tree.size 

zipped = single.zip tree
filtered = filterTheTwoZipped zipped

sumT = 0
sumS = 0
sumH = 0
filtered.each { |a| 
  puts a.join ' '
  sumS += a[1]
  sumT += a[2]
  sumH += a.min
}

puts "filtered: #{filtered.size}"
puts "avg time single: #{sumS/filtered.size} tree: #{sumT/filtered.size} (ratio: #{sumT/sumS}) hybrid: #{sumH/filtered.size}"
