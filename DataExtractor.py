import sys
import json
import unicodedata
import gzip
import time, datetime
import sqlite3 as sqlite
import struct
import sys 

def unusedDBcode():
	# Create a DB handle and a cursor
	dbname = "tweet.sqlite"
	if(len(sys.argv)==4):
		dbname = sys.argv[3]
	con = sqlite.connect(dbname)
	cur = con.cursor()
	cur.execute('CREATE  TABLE  IF NOT EXISTS "tweets" ("tid" INTEGER NOT NULL, "txt" TEXT, x INTEGER, y INTEGER, time INTEGER, "size" INTEGER)')
	con.commit()
	stringTime = 'Mon Feb 13 16:56:28 +0000 2012'
	datetime.datetime.fromtimestamp(time.mktime(time.strptime(stringTime, time_format)))
	time_format = "%a %b %d %H:%M:%S +0000 %Y"

def addTweet(tid,txt,x,y,time,size):
	q = "insert into tweets values (?,?,?,?,?,?)"
	cur.execute(q,(tid,txt,x,y,time,size))
	con.commit()
	cur.close()
	con.close()	

#remove non-printabtle values of a string --> now we can redirect and pipe
def printable(input):
    return unicodedata.normalize('NFKD', input).encode('ascii','ignore').replace('\n', ' ').replace('\r', ' ')

#print all attributes and values of json:
def printAttAndVal(js):
	for att,val in js.iteritems():
		print att, val

def getSecondsFromEpoch(timeStr):
	time_format = "%a %b %d %H:%M:%S +0000 %Y"
	d = datetime.datetime.fromtimestamp(time.mktime(time.strptime(timeStr, time_format)))
	return int(time.mktime(d.timetuple()))


f = gzip.open(sys.argv[1],'r') #assuming it is a json and gzipped, no check
for line in f:
	js = json.loads(line)
	if(js['retweet_count']>0):
		sys.stderr.write('retweet: '+str(js['retweet_count']) + "\n")
	if(js['geo']['type'] != 'Point'):
		sys.stderr.write('not point')
	dic = { 'id' : js['id'], 'geo' : js['coordinates'][u'coordinates'], 'time' : getSecondsFromEpoch(js['created_at']), 'text' : printable(js['text']) }

	print dic['id'],dic['geo'][0],dic['geo'][1],dic['time'],len(dic['text'].split()),dic['text']
f.close()



