/*
 * registry.h
 *
 *  Created on: Apr 22, 2012
 *      Author: sergeyn
 */

#ifndef REGISTRY_H_
#define REGISTRY_H_

#include <unordered_map>

class registry :  public std::unordered_map<std::string, std::string>{
	registry() {}
	registry(const registry& r) : std::unordered_map<std::string, std::string>(r){}
	registry& operator=(const registry&) { return *this; }
	static registry* ptr;
public:
	inline static registry& getInstance() { if(nullptr == ptr) ptr = new registry(); return *ptr; }
};


#endif /* REGISTRY_H_ */
