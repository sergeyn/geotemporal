/*
 * Merger.cpp
 *
 *  Created on: Mar 11, 2012
 *      Author: sergeyn
 */

#include <thread>
#include "Merger.h"

const std::string Merger::suffix(".mrg0");

bool fexists(const std::string& filename) {
  std::ifstream ifile(filename);
  return ifile;
}


//T should be a mergeList - front,next and push_back
//T should be sortable - t1<t2 iff t1->head<t2->head
template <typename T>
class mergeList {
public:
	virtual const T& front() const = 0;
	virtual bool next() = 0;
	virtual void push_back(const T& v) = 0;
	bool operator<(mergeList<T>& other) { return front() < other.front(); }
	virtual ~mergeList(){}
};

template <typename T, typename S>
class fstreamOfTuples : public mergeList<T>{
	T current;
	S* stream;
	std::ios_base::openmode mode;

public:
	fstreamOfTuples(S* strm,  std::ios_base::openmode md=std::ios_base::in) : stream(strm),mode(md) { next();	};
	virtual ~fstreamOfTuples() {}
	const T& front() const {return current;}
	bool next() {
		if( mode == std::ios_base::out ||  (! stream->good()) )
			return false;

		char buffer[6048];
		stream->getline(buffer,6000); //if not enough... you will miss some
		current = T::fromString(std::string(buffer));
		return (current.docid>0);
	}

	void push_back(const T& v) {
		if( mode == std::ios_base::in)
			THROW("file open issue");
		(*stream) << v;
	}
};

template<typename T>
void multiMerge(std::vector<mergeList<T>* >& lists, mergeList<T>& res)  {
	size_t count = 0;
	auto cmp = [](mergeList<T>* a, mergeList<T>* b){ return !(*a < *b); };
	std::make_heap(lists.begin(), lists.end(), cmp);

	while(lists.size()) {
		if((count++)%1000000==0)
			std::cout << count << std::endl;
		//get the smallest
		std::pop_heap(lists.begin(),lists.end(),cmp);
		auto pivot = lists.back();
		lists.pop_back();

		res.push_back(pivot->front()); //put it to results
		if(pivot->next()) {
			lists.push_back(pivot); //put it back to heap
			std::push_heap(lists.begin(),lists.end(),cmp);
		}
	}
}

void Merger::merge(const std::string& IndexOutputFileName) {
	if(files.size() < 2) {
		std::cerr << "merge for less than 2 files is not supported :)" << std::endl;
		THROW("");
	}

	std::fstream out;
	out.open(IndexOutputFileName, std::ios_base::out);
	if(out.is_open())
			std::cout << "Starting merge " << files.size() << " files to " << IndexOutputFileName << std::endl;
	else {
			std::cerr << "Could not open an output file handle" << std::endl;
			return;
	}
	fstreamOfTuples<termTuple,std::fstream> outl(&out,std::ios_base::out);

	std::vector<mergeList<termTuple>* > mlists;
	std::vector<fstreamOfTuples<termTuple,std::fstream> > mergelists;
	std::vector<std::fstream*> vstreams;
	for(std::string fname : files) {
		std::fstream* st = new std::fstream;
		std::cout << "open handle to file: " <<  fname+suffix << std::endl;
		st->open(fname+suffix, std::ios_base::in);
		if(! st->is_open())
			THROW("merge can't open");
		vstreams.push_back(st);
		mergelists.push_back(fstreamOfTuples<termTuple,std::fstream>(st,std::ios_base::in));
	}

	std::vector<mergeList<termTuple>* > lists;
	for(fstreamOfTuples<termTuple,std::fstream>& m : mergelists)
		lists.push_back(&m);

	multiMerge(lists,outl);

	for(std::fstream* ptr : vstreams)
		delete ptr;
}

Merger::Merger(absDocIdMapper& mapp, const std::vector<std::string>&  f, const std::string& p)
			: path(p), mapper(mapp) {
	files = f;
	//std::transform(files.begin(), files.end(), files.begin(), addPath);
}


void Merger::reorderDocIds(const std::vector<bignum>& oldToNewIds) const {
	auto filesIt = files.begin();
	while(filesIt != files.end()) {
		std::cout << "reorder files left: " << files.end()-filesIt << std::endl;
		std::vector<std::thread> ts;
		for(unsigned int  i=0; i<MRGTREADSCOUNT && filesIt != files.end(); ++i) {
			//create a thread that would run method preprocessOne with AsciiStreamer and TweetParser -- pass it the filename
			ts.push_back(std::thread(&Merger::_OneReorderDocIds,this,*filesIt, oldToNewIds));
			++filesIt;
		}
		for(std::thread& t : ts)
			t.join();
	}
}



void  Merger::_OneReorderDocIds(const std::string& fname, const std::vector<bignum>& oldToNewIds) const {
	std::fstream out;
	out.open(fname+suffix+"n", std::ios_base::out);
	if(!out.is_open())
		THROW("file open issue");
	fstreamOfTuples<termTuple,std::fstream> outl(&out,std::ios_base::out);

	std::fstream in;
	in.open(fname+suffix, std::ios_base::in);
	if(! in.is_open())
		THROW("file open issue");
	fstreamOfTuples<termTuple,std::fstream> inl(&in,std::ios_base::in);

	std::vector<termTuple> tuples;
	while(inl.next()) {
		termTuple t = inl.front();
		t.docid = oldToNewIds[t.docid];
		tuples.push_back(t);
	}
	std::sort(tuples.begin(),tuples.end());
	for(const termTuple& t : tuples)
		outl.push_back(t);

	out.close();
	std::string cmd = "mv " + fname + suffix + "n " + fname + suffix; //you didn't see that...
	std::cout << system(cmd.c_str()) << " returned by mv " << __FILE__ << __LINE__ << std::endl;
}

#define NOTHREADS
void Merger::preprocess() {
	auto filesIt = files.begin();
	while(filesIt != files.end()) {
		std::cout << "preprocessing files left: " << files.end()-filesIt << std::endl;
		std::vector<std::thread> ts;
		for(unsigned int  i=0; i<MRGTREADSCOUNT && filesIt != files.end(); ++i) {
			#ifdef NOTHREADS
				std::cout << "prpr " << *filesIt << std::endl;
				Merger::preprocessOne<PARSETYPES>(*filesIt);
			#else
				//create a thread that would run method preprocessOne with AsciiStreamer and TweetParser -- pass it the filename
				ts.push_back(std::thread(&Merger::preprocessOne<PARSETYPES>,this,*filesIt));
			#endif
			++filesIt;
		}
		for(std::thread& t : ts)
			t.join();
	}
}


