/*
 * RawDocument.cpp
 *
 *  Created on: Mar 11, 2012
 *      Author: sergeyn
 */

#include "RawDocument.h"


inline char easyFilter(char in){ //very agressive filter -- leaving just the english letters,numbers,@#
        return (isalnum(in) || in=='#' || in=='@') ? tolower(in) : ' ';
}
inline bool goodTerm(const std::string& term) {
	return !(term.find("http://") != std::string::npos || term.find("https://") != std::string::npos);
}

std::string rmRedund(std::string& term) {
	if (term[0] == '@' || term[0] == '#')
		return term;

	size_t idx(0), runSize(0);
	std::stringstream resultBuf;

	while (idx < term.size() - 1) 	{
		if (runSize < 2)
			resultBuf << term[idx];

		if( !isdigit(term[idx]) && term[idx] == term[idx+1])
			runSize += 1;
		else
			runSize = 0;
		idx += 1;
	}

	if (runSize < 2)
		resultBuf << term[term.size()-1];
	return resultBuf.str();
}

std::vector<std::string> sanitize(std::vector<std::string>::iterator it, std::vector<std::string>::iterator end){
	std::vector<std::string> ret;
	for (;it!=end; ++it) {
		auto& f = *it;
		if (goodTerm(f)) {
			std::transform(f.begin(), f.end(), f.begin(), easyFilter);
			std::vector<std::string> tokens = tokenize(f);
			for (std::string& s : tokens)
				ret.push_back(rmRedund(s));
		}
	}
	return ret;
}

std::vector<std::string> TweetParser::getMeta(std::string& raw, docTuple& meta) {
	docTuple d;
	std::vector<std::string> naiveV = tokenize(raw);
	meta.seconds = atol(naiveV[3].c_str());
	meta.location = gPoint( atof(naiveV[1].c_str()),atof(naiveV[2].c_str()) );
	meta.url = naiveV[0];
	return naiveV;
}

std::vector<std::string> TweetParser::operator()(std::string raw, docTuple& meta) { //why copy of raw? check this TODO
	const unsigned int META(5);
	std::vector<std::string> naiveV = getMeta(raw,meta);
	std::vector<std::string> normalized = sanitize(naiveV.begin()+META,naiveV.end());
	abort(); //fix:
	//meta.size = normalized.size();


	//if(naiveV.size()<=META)return {}; //empty tweet - will have a 0-sized doc in .len

	std::vector<std::string> ret;
	ret.reserve(normalized.size());
	for(std::string& s : normalized)
		ret.push_back(s);
	return ret;
}

docTuple DocIdMapper::giveNextId(const std::string&) {
		std::lock_guard<std::mutex> lk(sharedMu);
		docTuple d(docIdToDocTuple.size());
		docIdToDocTuple.push_back(d);
		return d;
	}

void DocIdMapper::atomicUpdTuple(docTuple& doc) {
	std::lock_guard<std::mutex> lk(sharedMu);
	docIdToDocTuple[doc.docid]=doc;
}

docTuple presetDocIdMapper::giveNextId(const std::string& url) {
	return (url == "") ? docTuple() : tweetToDocId[url];
}

void presetDocIdMapper::atomicUpdTuple(docTuple& doc) {
	doc.docid = tweetToDocId[doc.url];
	//docIdToDocTuple[docToIndex[doc.docid]].size = 0; // doc.size;
}

RawDocument::RawDocument(docTuple& doc, const std::vector<std::string>& words) : docT(doc) {
	for(auto it = words.begin(); it != words.end(); ++it) {
		if(terms.find(*it) == terms.end())
			terms[*it]=1;
		else
			++terms[*it];
	}
}

void RawDocument::print() const {
	for(auto it = terms.begin(); it != terms.end(); ++it)
		std::cout << (*it).first  << " " <<  docT.docid << " " << (*it).second << std::endl;
}

void RawDocument::addYourTermsToPool(std::vector<termTuple>& pool) const {
	for(auto it = terms.begin(); it != terms.end(); ++it) {
		termTuple tp((*it).first,docT.docid, (*it).second );
		pool.push_back(tp);
	}
}
