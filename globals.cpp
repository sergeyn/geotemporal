/*
 * globals.cpp
 *
 *  Created on: Mar 15, 2012
 *      Author: sergeyn
 */

#include "globals.h"
registry* registry::ptr(nullptr);

std::vector<std::string> tokenize(const std::string& str,
	      const std::string& delimiters, const bool trimEmpty) {
	std::vector<std::string> tokens;
	std::string::size_type pos, lastPos = 0;
	while(true)
	{
		pos = str.find_first_of(delimiters, lastPos);
		if(pos == std::string::npos)
		{
			pos = str.length();

			if(pos != lastPos || !trimEmpty)
				tokens.push_back(std::string(str.data()+lastPos,
						(std::string::size_type)pos-lastPos ));

			break;
		}
		else
		{
			if(pos != lastPos || !trimEmpty)
				tokens.push_back(std::string(str.data()+lastPos,
						(std::string::size_type)pos-lastPos ));
		}

		lastPos = pos + 1;
	}
	return tokens;
}
