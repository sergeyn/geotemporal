/*
 * Merger.h
 *
 *  Created on: Mar 11, 2012
 *      Author: sergeyn
 */

#ifndef MERGER_H_
#define MERGER_H_

#include "globals.h"
#include "RawDocument.h"

bool fexists(const std::string& filename);

class Merger {
	std::vector<std::string> files;
	std::string path;
	absDocIdMapper& mapper;
	static const std::string suffix;



	template <typename StreamT, typename ParserT>
	void preprocessOne(const std::string& fname) {
		//if(fexists(fname+suffix)) return; -- development time optimization...
		StreamT stream(fname);

		std::vector<termTuple> tuples;
		std::string chunk = "";
		ParserT parser;
		while("" != (chunk = stream.getChunk())) {
			docTuple docT = mapper.giveNextId();
			auto v = parser(chunk,docT);
			RawDocument doc(docT, v);
			mapper.atomicUpdTuple(docT);
			assert(docT.docid); //alas, my love, you do me wrong!
			doc.addYourTermsToPool(tuples);

		}

		std::sort(tuples.begin(), tuples.end());
		std::ofstream fout(stream.getFname()+suffix); //zero level in merging
		for(const auto& t : tuples)
			fout << t;
	}

public:
	Merger(absDocIdMapper& mapper, const std::vector<std::string>&  f, const std::string& path); //list of input files
	void preprocess(); //dump each file into mergeable state
	void reorderDocIds(const std::vector<bignum>& oldToNewIds) const;
	void _OneReorderDocIds(const std::string& fname, const std::vector<bignum>& oldToNewIds) const;
	void merge(const std::string& IndexOutputFileName);
};


#endif /* MERGER_H_ */
