USERNM := $(shell whoami)
MKTO := /dev/shm/$(USERNM)/obj
DIRS    := .
SOURCES := $(foreach dir, $(DIRS), $(wildcard $(dir)/*.cpp))
#SOURCES := $(wildcard *.cpp) #override
OBJS    := $(patsubst %.cpp, %.o, $(SOURCES))
OBJS    := $(foreach o,$(OBJS),$(MKTO)/$(o))
DEPFILES:= $(patsubst %.o, %.d, $(OBJS))

HOST:=$(shell hostname)

#For some versions of eclipse: add __GXX_EXPERIMENTAL_CXX0X__ 
#Add to older eclipse versions:  __GXX_EXPERIMENTAL_CXX0X__ and __GXX_EXPERIMENTAL_CXX0X__
#Since Kepler: Project -> Propertirs ->  C/C++ General -> Preprocessor Include Paths, Macros etc. -> providers -> gcc built-int settings 
#change to: ${COMMAND} -E -P -v -dD "${INPUTS}" -std=c++11
STD=-std=c++11 -D CPP0X
#STD=-std=c++0x -D CPP0X

#http://blog.httrack.com/blog/2014/03/09/what-are-your-gcc-flags/
DBG=$(STD) -g3 -D DEBUG -Wall -Wextra  -pipe -Wno-unused-parameter
RLS=$(STD) -O3 -D NDEBUG -Wall -D TIMING  -Wextra -pipe -fno-exceptions -Wl,--discard-all  -Wl,--no-undefined

# -flto 
CFLAGSOPT = $(RLS) -include precompiled_inc.h  -Wfatal-errors -fno-rtti -Wno-sign-compare  -c -MMD -march=native 
CFLAGSDBG = $(DBG) -include precompiled_inc.h -c  -MMD  -march=native

#-flto
LFLAGSOPT = -O3 
LFLAGSDBG = -g3 -D DEBUG

mkdebug ?= 0
ifeq ($(mkdebug),0)
	CFLAGS = $(CFLAGSOPT)
	LFLAGS = $(LFLAGSOPT)
else
	CFLAGS = $(CFLAGSDBG)
	LFLAGS = $(LFLAGSDBG)
endif

mk32 ?= 0
ifeq ($(mk32),1)
	CCFLAGS = $(CFLAGS) -m32
	LLFLAGS = $(LFLAGS) -m32
else
	CCFLAGS = $(CFLAGS) 
	LLFLAGS = $(LFLAGS) 
endif

#use gGet script to select the best g++
COMPILER:=$(shell ./gGet.sh)

.SILENT:

#link the executable
QPBIN := queryP
$(QPBIN): $(OBJS)  
	#$(COMPILER) $(LLFLAGS) -o buildI mainB.o $(OBJS) -pthread -lz 
	$(COMPILER) $(LLFLAGS)  $(OBJS)  -o queryP -pthread -lz
	echo "Run '$(QPBIN)'"
	echo "---- FIN ----"


precompiled_inc.h.gch : precompiled_inc.h
	$(COMPILER) $(CCFLAGS) -o $@ $<

#generate dependency information and compile
$(MKTO)/%.o : %.cpp precompiled_inc.h.gch
	@mkdir -p $(@D)
	echo $@
	$(COMPILER) $(CCFLAGS) -o $@ -MMD $<	

#remove all generated files
clean:
	rm -rf $(QPBIN) buildI $(MKTO)/* core Options.h


#include the dependency information
-include $(DEPFILES)
