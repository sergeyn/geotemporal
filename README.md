# README #

Code for experimental evaluation of an index to support geo-temporal+keyword queries

Sergey Nepomnyachiy, Bluma Gelley, Wei Jiang, Tehila Minkus:
What, where, and when: keyword search with spatio-temporal ranges. GIR 2014: 2
