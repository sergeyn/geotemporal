#include "TreeIndexer.h"
#include "Indexer.h"
#include "InvertedList.h"
#include "QP.h"
#include "globals.h"
#include "profiling.h"
#ifdef SERVER
#include <boost/asio.hpp>
using boost::asio::ip::tcp;
#endif

#ifdef BUILDER
int main_build(int argc, char* argv[]) {
	if(argc<2) {
		std::cout << "Usage: " << argv[0] << " allDocs-file" << std::endl;
		return -1;
	}
	std::string root = argv[1]; //"/data1/team/geotemporal/test/allDocs";
	buildTreeIndex<PARSETYPES>(root);
	std::ifstream treeserialized(root+".tree");
	return 0;
}
#endif

//----------------------------------------------------------------------------------------------
class queryManager {
	QP qp;
	profilerC& p;
	const int ALLQS;
	const unsigned int topK;
	bignum MAXD;

public:
	queryManager(const std::string& indexpath, unsigned int tK, bignum globalMAXD) :
		qp(indexpath,tK), p(profilerC::getTheInstance()), ALLQS(3), topK(tK), MAXD(globalMAXD) {
		p.add("all-queries",ALLQS);
		p.add("startQ",5);
		p.initNewCounter(); p.initNewCounter();

	}
	~queryManager() { p.printReport();}

	std::pair<double,results> query(const queryTuple& query) {
		std::vector<INVLIST> lists;
		qp.setStartEndDs(qp.getStartD(query.timeR.left),qp.getMaxD(query.timeR.right));
		qp.openlists(query.terms,lists);
		results res;
		double timeTook = 0;
		if(lists.size()==query.terms.size()) {
			p.start(ALLQS);
			res = qp.andQuery(lists,query); //do AND query
			timeTook = p.end(ALLQS)/1000.0;
		}
		return std::make_pair(timeTook,filterResults(res,query));
	}

	results filterResults(results& res, const queryTuple& qtp) {
		results san;
		for(result& f : res) {
			const docTuple& dt = qp.getDocMeta(f.did);
			if(f.score>0.001 && qtp.geoR.isPointInRange(dt.location) && qtp.timeR.isTimeInRange(dt.seconds)) {
				f.url = dt.url;
				f.location = dt.location;
				f.timestamp = dt.seconds;
				san.push_back(f);
			}
		}
		#ifndef SERVER
		//if(san.size()) 	std::cout << /*"TOP-"<< topK << " results:\n " << */ san << std::endl;
		#ifdef DEBUG
		//else	std::cout << "NORES" << std::endl;
		#endif
		#endif
		return san;
	}
};
//----------------------------------------------------------------------------------------------
inline std::string rprocess(const std::string& req) {
	size_t pos = req.find("GET /?southWestLng=");
	size_t posEnd = req.find("&nil");
	if(std::string::npos == pos)
		return "";
	std::string params = req.substr(pos,posEnd);
	//std::cout << "request #" << ++count << std::endl << params.substr(params.find("?")+1)  << std::endl;
	return params.substr(params.find("?")+1);
}
//----------------------------------------------------------------------------------------------
#ifdef SERVER
int main_server(int argc, char* argv[]) {
	if(argc<3) {
		std::cout << "Usage: " << argv[0] << " .tree-path port [topk]" << std::endl;
		return -1;
	}

	int topk = 1024;
	if(argc==4)
		topk = atoi(argv[3]);
	int port = atoi(argv[2]);
	std::string indexpath(argv[1]);
	std::ifstream treeserialized(indexpath);
	std::ifstream streeserialized(SHADOWALLDOCS+".tree");
	if(! (treeserialized.is_open() && streeserialized.is_open() ) ) {
		std::cerr << "could not open tree file" << std::endl;
		return -1;
	}
	bignum docsNumInTree=16718359;
	bignum docsNumInShadowTree=16718359;
	WeiTree<docTuple, Indexer>* shadowTree = WeiTree<docTuple, Indexer >::loadFromStream(streeserialized, docsNumInShadowTree);
	streeserialized.close();
	WeiTree<docTuple, Indexer>* tree = WeiTree<docTuple, Indexer >::loadFromStream(treeserialized, docsNumInTree);

	//get all leafnodes:
	std::unordered_map<std::string,queryManager*> indices;
	for(const std::string& path : tree->getAllLeafPaths())
		indices[path] = new queryManager(path+"/index",topk,docsNumInTree);
	for(const std::string& path : shadowTree->getAllLeafPaths())
		indices[path] = new queryManager(path+"/index",topk,docsNumInShadowTree);


	//queryManager qm(indexpath,topk,16718359);
	boost::asio::io_service io_service;
	try {
		// This sets up a connection to localhost
		tcp::endpoint endpoint(tcp::v4(), port);
		tcp::acceptor acceptor(io_service, endpoint);
		std::cerr << "Starting server" << std::endl;
		while(true)	{
			tcp::socket socket(io_service);
			acceptor.accept(socket);
			boost::system::error_code ignored_error;
			std::vector<char> queryB(4096);
			socket.read_some(boost::asio::buffer(queryB), ignored_error);
			std::string req(&queryB[0]); //GET /?keywords=a,b,c&x1=1&y1=1&x2=2&y2=2&start=0&end=4
			if(req.find("APND") == 0) { //append operation requested via POST
				for(const std::string& path : shadowTree->getAllLeafPaths())
						delete indices[path];
				delete shadowTree; //release file handles as well

				updateShadowManager({req.substr(4)},docsNumInTree);
				streeserialized.close();
				streeserialized.open(SHADOWALLDOCS+".tree");
				if(! streeserialized.is_open())
					throw "ouch";

				shadowTree = WeiTree<docTuple, Indexer >::loadFromStream(streeserialized, docsNumInShadowTree);
				for(const std::string& path : shadowTree->getAllLeafPaths()) {
					indices[path] = new queryManager(path+"/index",topk,docsNumInShadowTree);
				}
			}

			std::string chunk = rprocess(req);
			results mergedres;
			if(chunk != "") {
				queryTuple qtuple = queryTuple::fromWebString(chunk);
				for(const std::string& path : tree->getIntersectingLeafPaths(qtuple.geoR)) {
					results tmp = indices[path]->query(qtuple);
					for(result sr : tmp)
						mergedres.push_back(sr);
				}
				for(const std::string& path : shadowTree->getIntersectingLeafPaths(qtuple.geoR)) {
					results tmp = indices[path]->query(qtuple);
					for(result sr : tmp)
						mergedres.push_back(sr);
				}
			}
			std::ostringstream oss;
			oss << mergedres << std::endl;
			boost::asio::write(socket, boost::asio::buffer(oss.str()), boost::asio::transfer_all(), ignored_error);
		}
	}
	catch (std::exception& e) 	{
		std::cerr << e.what() << std::endl;
	}

	return 0;
}
#endif
//----------------------------------------------------------------------------------------------
inline int main_query(int argc, char* argv[]) {
	if(argc<4) {
		std::cout << "Usage: " << argv[0] << " .tree-path querylog mode=mem [topk->optional]" << std::endl;
		return -1;
	}

	int topk = 10;
	if(argc==5)
		topk = atoi(argv[4]);

	std::string fname = std::string(argv[2]);
	std::string mode = std::string(argv[3]);
	std::string indexpath(argv[1]);

	std::ifstream treeserialized(indexpath);
	bignum docsNumInTree = QP::globalMAXD = 4000124; //30000002;
	
	profilerC& p = profilerC::getTheInstance();
	const int ALLQS = 3;
	
	WeiTree<docTuple, Indexer>* tree = WeiTree<docTuple, Indexer >::loadFromStream(treeserialized,docsNumInTree);
	std::cout << "processing top-" << topk << " queries\n" << 
		 "postings indexed (hardcoded val): " << docsNumInTree << std::endl;
	//get all leafnodes:
	std::unordered_map<std::string,queryManager*> indices;
	for(const std::string& path : tree->getAllLeafPaths())
		indices[path] = new queryManager(path+"/index",topk,docsNumInTree);


	AsciiStreamer strm(fname); 
	std::string chunk;
	double totalTime = 0.0;
	unsigned countQs = 0;
	while("" != (chunk = strm.getChunk())) {
		queryTuple qtuple = queryTuple::fromString(chunk);

		std::cout << "$$ " << qtuple << " ";
		unsigned res = 0;
		double allTime = 0.0;
		//p.start(ALLQS); //wrong to measure here -- also gets the openlists...
		for(const std::string& path : tree->getIntersectingLeafPaths(qtuple.geoR)) {
			std::pair<double,results> r =  indices[path]->query(qtuple);
			res += r.second.size();
			allTime += r.first;
		}
		//p.end(ALLQS);
		std::cout << "| intersected " << tree->getIntersectingLeafPaths(qtuple.geoR).size() << " nodes and had "  << res << "  results in time: " << allTime  << std::endl;
		++countQs;
		totalTime += allTime;
	} //end while
	
	std::cout << totalTime/countQs << " ms. avg query time" << std::endl;
	profilerC::getTheInstance().printReport();
	return 0;
}
//----------------------------------------------------------------------------------------------
int main(int argc, char* argv[]) {
	return main_query(argc, argv);

#ifdef BUILDER
	return main_build(argc, argv);
#endif
#ifdef TEST
	return main_test(argc, argv);
#endif
#ifdef SERVER
	return main_server(argc, argv);
#endif
}
