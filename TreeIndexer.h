/*
 * TreeIndexer.h
 *
 *  Created on: Apr 23, 2012
 *      Author: sergeyn
 */

#ifndef TREEINDEXER_H_
#define TREEINDEXER_H_

#include "Indexer.h"
#include "WeiTree.h"

template <typename StreamT, typename ParserT>
bignum initAllDocsDocids(doclenMap& dmp, const std::string& fname, bignum startWith=0 ) {
	StreamT stream(fname);
	ParserT parser;
	std::string chunk = "";

	dmp.clear();
	dmp.reserve(1000000);
	dmp.push_back(docTuple());
	while("" != (chunk = stream.getChunk())) {
		docTuple docT;
		ParserT::getMeta(chunk,docT);
		dmp.push_back(docT);
	}
	docTuple::sortBySeconds(dmp.begin()+1,dmp.end());
	bignum id = startWith;
	for(docTuple& d : dmp)
		d.docid = id++;
	return id;
}

const std::string SHADOWALLDOCS("/tmp/shadow/allDocs");
//----------------------------------------------------------------------------------------------
inline bignum updateShadowManager(const std::vector<std::string>& chunks, bignum maxd) { //terribly non-efficient function. TODO: for dynamic group
	{
		std::ofstream f(SHADOWALLDOCS, std::ios::app );
		for(const std::string& tweet : chunks)
			f << tweet; // << std::endl;
		f.flush();
		f.close();
	}
	doclenMap dmp;
	bignum shadowMaxId = initAllDocsDocids<PARSETYPES>(dmp, SHADOWALLDOCS, maxd);
	std::cout << "new shadowmaxid: " << shadowMaxId << std::endl;

	WeiTree<docTuple, Indexer > t(0,SHADOWALLDOCS);
	t.buildTree(USABBOX,dmp,1,dmp.size()-1);
	std::ofstream serialize(SHADOWALLDOCS+".tree");
	t.printTree(serialize);
	return shadowMaxId;
}

//----------------------------------------------------------------------------------------------
template <typename StreamT, typename ParserT>
void buildTreeIndex(const std::string& allDocs) {

	doclenMap mp;
	initAllDocsDocids<StreamT,ParserT>(mp,allDocs);
	std::cout << "init loaded " << mp.size() << " doc-tuples" << std::endl;
	WeiTree<docTuple, Indexer > t(0,allDocs);
	//gRange WORLD(gPoint(-144.0,-22.0),gPoint(135.0, 78.0));
	t.buildTree(USABBOX,mp,1,mp.size()-1);
	std::ofstream serialize(allDocs+".tree");
	t.printTree(serialize);
}

#endif /* TREEINDEXER_H_ */
