/*
 * RawDocument.h
 *
 *  Created on: Mar 11, 2012
 *      Author: sergeyn
 */

#ifndef RAWDOCUMENT_H_
#define RAWDOCUMENT_H_

#include <mutex>
#include "globals.h"
#include "Tuples.h"

class absDocIdMapper {
public:
	virtual ~absDocIdMapper() {}
	virtual docTuple giveNextId(const std::string& url="") = 0;
	virtual void atomicUpdTuple(docTuple& doc)=0;
	virtual std::vector<docTuple>& getDocsMeta()=0;
};
//======================================================================================================
class DocIdMapper : public  absDocIdMapper{
	std::mutex& sharedMu;
	std::vector<docTuple> docIdToDocTuple;

public:
	explicit DocIdMapper(std::mutex& shM) : sharedMu(shM) {
		docIdToDocTuple.push_back(docTuple()); //push the zero-dummy-doc
	}
	virtual ~DocIdMapper() {}

	docTuple giveNextId(const std::string& url="");
	void atomicUpdTuple(docTuple& doc);
	std::vector<docTuple>& getDocsMeta() {return docIdToDocTuple; }
};
//======================================================================================================
class presetDocIdMapper : public  absDocIdMapper{
	std::vector<docTuple>& docIdToDocTuple;
	std::unordered_map<std::string, bignum> tweetToDocId;
	std::unordered_map<bignum, bignum> docToIndex;
public:
	explicit presetDocIdMapper(std::vector<docTuple>& presets) : docIdToDocTuple(presets)  {
		bignum i=0;
		for(const docTuple& d : presets) {
			tweetToDocId[d.url]=d.docid;
			docToIndex[d.docid]=i++;
		}
	}
	virtual ~presetDocIdMapper() {}

	docTuple giveNextId(const std::string& url="");
	void atomicUpdTuple(docTuple& doc);
	std::vector<docTuple>& getDocsMeta() {return docIdToDocTuple; }
};

//======================================================================================================
class BaseParser {
public:
	virtual ~BaseParser() {}
	virtual std::vector<std::string> operator()(std::string raw, docTuple& labelBuf) = 0;
};
//======================================================================================================
class TweetParser : BaseParser {
public:
	TweetParser() {}
	virtual std::vector<std::string> operator()(std::string raw, docTuple& meta);
	static std::vector<std::string> getMeta(std::string& raw, docTuple& meta);
};
//======================================================================================================
class RawDocument {
		docTuple& docT;
		mapStr2UI terms;
	public:
		RawDocument(docTuple& doc, const std::vector<std::string>& words=std::vector<std::string>());

		unsigned int size() const { return terms.size(); }
		void print() const;
		void addYourTermsToPool(std::vector<termTuple>& pool) const;
};

#endif /* RAWDOCUMENT_H_ */
