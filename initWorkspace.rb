def getNovemberURLs
	arrU = []
	days = (8...31).map { |day| day < 10 ? '0' + day.to_s : day.to_s }
	hours = (0..24).map { |h| h < 10 ? '0' + h.to_s : h.to_s }
	days.each { |day|
		hours.each { |hour| 
			arrU << "http://vgc.poly.edu/files/llins/twitter/2011-11-#{day}/twitter_us_2011-11-#{day}_#{hour}h.json.tar.gz"
		}
	}
	arrU
end
#-----------------------------------------------------------------------	
def getAprilURLs
	arrU = []
	days = (1...24).map { |day| day < 10 ? '0' + day.to_s : day.to_s }
	hours = (0..24).map { |h| h < 10 ? '0' + h.to_s : h.to_s }
	days.each { |day|
		hours.each { |hour| 
			arrU << "http://vgc.poly.edu/files/llins/twitter/2012-04-#{day}/twitter_us_2012-04-#{day}_#{hour}h.json.tar.gz"
		}
	}
	arrU
end
#-----------------------------------------------------------------------
def bringData(path, limit=1, workers=4)
	%x[mkdir -p #{path}]
	Dir.chdir path
	urls = getNovemberURLs
	#urls << getAprilURLs -- not supporting in this version
	
	tasks = urls[0...limit]
	workers = tasks.size if workers>tasks.size
	quota = tasks.size/workers
	arr = []
	(0...workers).each { |w|
		arr << Thread.new { tasks[w*quota...(1+w)*quota].each { |jsn| 
		    basename = jsn.split('/')[-1].split('.tar.gz')[0]
			%x[wget #{jsn} && tar xzvf #{basename}.tar.gz && rm #{basename}.tar.gz && gzip data/*/#{basename} && mv data/*/#{basename}.gz . ] 
		} 
	  }
	}
	
	tasks[workers*quota..-1].each { |jsn| 
		    basename = jsn.split('/')[-1].split('.tar.gz')[0]
			%x[wget #{jsn} && tar xzvf #{basename}.tar.gz && rm #{basename}.tar.gz && gzip data/*/#{basename} && mv data/*/#{basename}.gz . ] 
	}
	arr.each {|t| t.join}

end
#-----------------------------------------------------------------------
def extractData(path,workers=4)
	tasks = Dir[path+'/twitter_us_20*h.json.gz']
	workers = tasks.size if workers>tasks.size
	quota = tasks.size/workers
	arr = []
	(0...workers).each { |w|
		arr << Thread.new { tasks[w*quota...(1+w)*quota].each { |jsn|  
			puts "parsing #{jsn}"
			puts %x[python DataExtractor.py #{jsn} > #{jsn}.parsed]
		} 
	  }
	}
	tasks[workers*quota..-1].each { |jsn|  
		puts "parsing #{jsn}"
		%x[python DataExtractor.py #{jsn} > #{jsn}.parsed] }
	arr.each {|t| t.join}	
end
#-----------------------------------------------------------------------
def concatenateParsed(path)
	%x[rm -f #{path}/allDocs]
	Dir[path+'/twitter_us_20*h.json.gz.parsed'].sort.each { |prs| 
		puts "adding parsed data from #{prs}" 
		%x[cat #{prs} >> #{path}/allDocs] 
	}
end
#-----------------------------------------------------------------------
def cleanup wd
	%x[rm -rf #{wd}/data #{wd}/twit* #{wd}/work* #{wd}/index #{wd}/index.asc]
end
#-----------------------------------------------------------------------
def prepareShadow wd
	%x[rm -rf /tmp/shadow]
	%x[mkdir -p #{wd}/shadow && head -20 #{wd}/allDocs > #{wd}/shadow/allDocs]
	%x[./buildI #{wd}/shadow/allDocs && ln -s #{File.expand_path(wd)}/shadow /tmp/shadow] 
end
#-----------------------------------------------------------------------
def buildIndex wd
	%x[./buildI #{wd}/allDocs]
end
#-----------------------------------------------------------------------
def produceMapping wd,alldocs
	f=File.new(alldocs)
	arr = []
	arr <<  0
	f.each { |l|
        	arr << l.split()[0].to_i
	        arr << f.pos
	}

	STDERR.puts 'done readig raw'
	gf = File.open("#{wd}/mapIdOToffset#{arr.size/2}", "wb")
	gf.write arr.pack("Q#{arr.size}")
	gf.flush
	gf.close
end
#-----------------------------------------------------------------------

if ARGV.size < 3
	puts "usage: #{__FILE__} work-dir limit-of-files number-of-workers"
	exit -1
end	
d = Dir.pwd
workd = ARGV[0]
bringData workd,ARGV[1].to_i,ARGV[2].to_i 
Dir.chdir d
extractData workd
concatenateParsed workd
prepareShadow workd
buildIndex workd
produceMapping workd, workd+'/allDocs'
cleanup workd
puts "now you can use startServers.sh script from /home/ubuntu"
